<?php

namespace App\Exports;

use App\Models\BayiBLModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BayiBLExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct(int $year)
    {
        $this->year = $year;
    }
    public function headings(): array
    {

        return ["Tanggal", "Nama", "Alamat", "Subject", "AS", "BB", "PB", "Suhu", "LIKA", "LILA", "Anus", "KU", "Status Gizi", "SDIDTK", "MTBM", "MTBS", "Imunisasi", "TX", "KIE"];
    }


    public function collection()
    {
        $bayibl = BayiBLModel::join('pasiens', 'pemeriksaanbayibl.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaanbayibl.tanggal, pasiens.nama, pasiens.alamat, pemeriksaanbayibl.subject, pemeriksaanbayibl.as, pemeriksaanbayibl.berat_badan, pemeriksaanbayibl.pb, pemeriksaanbayibl.suhu, pemeriksaanbayibl.lika, pemeriksaanbayibl.lila, pemeriksaanbayibl.anus, pemeriksaanbayibl.ku, pemeriksaanbayibl.status_gizi, pemeriksaanbayibl.sdidtk, pemeriksaanbayibl.mtbm, pemeriksaanbayibl.mtbs, pemeriksaanbayibl.imunisasi, pemeriksaanbayibl.tx, pemeriksaanbayibl.kie '))
            ->whereRaw("YEAR(pemeriksaanbayibl.tanggal) = $this->year")
            ->get();

        return $bayibl;
    }
}
