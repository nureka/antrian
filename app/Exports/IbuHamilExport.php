<?php

namespace App\Exports;

use App\Models\IbuHamilModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class IbuHamilExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(int $year)
    {
        $this->year = $year;
    }
    public function headings(): array
    {

        return ["Tanggal", "Nama", "Alamat", "Umur", "SUbject", "Berat Badan", "LILA", "TD", "Nadi", "Suhu", "TFU", "Denyut Jantung", "LET", "LAB", "Skor", "G", "TX", "KIE"];
    }

    public function collection()
    {
        $ibuhamil = IbuHamilModel::join('pasiens', 'pemeriksaanibuhamil.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaanibuhamil.tanggal, pasiens.nama, pasiens.alamat, TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun, pemeriksaanibuhamil.subject, pemeriksaanibuhamil.berat_badan, pemeriksaanibuhamil.lila, pemeriksaanibuhamil.tekanan_darah, pemeriksaanibuhamil.nadi, pemeriksaanibuhamil.suhu, pemeriksaanibuhamil.tinggi_fundus_uteri, pemeriksaanibuhamil.denyut_jantung, pemeriksaanibuhamil.let, pemeriksaanibuhamil.lab, pemeriksaanibuhamil.skor, pemeriksaanibuhamil.g,  pemeriksaanibuhamil.tx, pemeriksaanibuhamil.kie'))
            ->whereRaw("YEAR(pemeriksaanibuhamil.tanggal) = $this->year")
            ->get();

        return $ibuhamil;
    }
}
