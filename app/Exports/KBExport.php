<?php

namespace App\Exports;

use App\Models\KBModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class KBExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(int $year)
    {
        $this->year = $year;
    }
    public function headings(): array
    {

        return ["Tanggal", "Nama", "Alamat", "Umur", "Subject", "BB", "TD", "Akseptor KB", "TX", "KIE"];
    }

    public function collection()
    {
        $kb = KBModel::join('pasiens', 'pemeriksaankb.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaankb.tanggal, pasiens.nama, pasiens.alamat, TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun, pemeriksaankb.subject, pemeriksaankb.berat_badan, pemeriksaankb.tekanan_darah, pemeriksaankb.akseptor_kb, pemeriksaankb.tx, pemeriksaankb.kie'))
            ->whereRaw("YEAR(pemeriksaankb.tanggal) = $this->year")
            ->get();

        return $kb;
    }
}
