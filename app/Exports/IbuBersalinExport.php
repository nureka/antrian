<?php

namespace App\Exports;

use App\Models\IbuBersalinModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class IbuBersalinExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(int $year)
    {
        $this->year = $year;
    }
    public function headings(): array
    {

        return ["Tanggal", "Nama", "Alamat", "Umur", "Partograf", "Kala 1", "Proses Persalinan", "TD", "Nadi", "Suhu", "CU", "Perineum", "Perdarahan", "TX", "BBL", "BB", "PB", "LIKA", "IMD"];
    }
    public function collection()
    {
        $ibubersalin = IbuBersalinModel::join('pasiens', 'pemeriksaanbersalin.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaanbersalin.tanggal, pasiens.nama, pasiens.alamat, TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun, pemeriksaanbersalin.partograf, pemeriksaanbersalin.kala_1, pemeriksaanbersalin.proses_persalinan, pemeriksaanbersalin.tekanan_darah, pemeriksaanbersalin.nadi, pemeriksaanbersalin.suhu, pemeriksaanbersalin.nadi, pemeriksaanbersalin.cu, pemeriksaanbersalin.perineum, pemeriksaanbersalin.perdarahan, pemeriksaanbersalin.tx, pemeriksaanbersalin.bbl,  pemeriksaanbersalin.bb, pemeriksaanbersalin.pb, pemeriksaanbersalin.lika, pemeriksaanbersalin.imd'))
            ->whereRaw("YEAR(pemeriksaanbersalin.tanggal) = $this->year")
            ->get();

        return $ibubersalin;
    }
}
