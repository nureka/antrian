<?php

namespace App\Exports;

use App\Models\AnakModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class AnakExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct(int $year)
    {
        $this->year = $year;
    }
    public function headings(): array
    {

        return ["Tanggal", "Nama", "Alamat", "Umur", "Subject", "BB", "TB", "TD", "Suhu", "RR", "Nadi", "Status Gizi", "MTBS", "Imunisasi", "TX", "KIE"];
    }

    public function collection()
    {
        $anak = AnakModel::join('pasiens', 'pemeriksaananak.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaananak.tanggal, pasiens.nama, pasiens.alamat, CONCAT(TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )," Tahun ",TIMESTAMPDIFF( MONTH, pasiens.tanggal_lahir, now() ) % 12, " bulan ", 
            FLOOR( TIMESTAMPDIFF( DAY, pasiens.tanggal_lahir, now() ) % 30.4375 ), " hari") as umur, pemeriksaananak.subject, pemeriksaananak.berat_badan, pemeriksaananak.tinggi_badan, pemeriksaananak.tekanan_darah, pemeriksaananak.suhu, pemeriksaananak.rr, pemeriksaananak.nadi, pemeriksaananak.status_gizi, pemeriksaananak.mtbs, pemeriksaananak.imunisasi, pemeriksaananak.tx, pemeriksaananak.kie '))
            ->whereRaw("YEAR(pemeriksaananak.tanggal) = $this->year")
            ->get();

        return $anak;
    }
}
