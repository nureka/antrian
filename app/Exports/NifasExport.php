<?php

namespace App\Exports;

use App\Models\NifasModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class NifasExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(int $year)
    {
        $this->year = $year;
    }
    public function headings(): array
    {

        return ["Tanggal", "Nama", "Alamat", "Umur", "Subject", "TD", "Nadi", "Suhu", "CU", "TFU", "Pendarahan", "Lochea", "BAB", "BAK", "Asi", "Post Partum", "TX", "KIE"];
    }

    public function collection()
    {
        $nifas = NifasModel::join('pasiens', 'pemeriksaannifas.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaannifas.tanggal, pasiens.nama, pasiens.alamat, TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun, pemeriksaannifas.subject, pemeriksaannifas.tekanan_darah, pemeriksaannifas.nadi, pemeriksaannifas.suhu, pemeriksaannifas.nadi, pemeriksaannifas.cu, pemeriksaannifas.tfu, pemeriksaannifas.pendarahan, pemeriksaannifas.lochea, pemeriksaannifas.bab,  pemeriksaannifas.bak, pemeriksaannifas.asi, pemeriksaannifas.post_partum, pemeriksaannifas.tx, pemeriksaannifas.kie'))
            ->whereRaw("YEAR(pemeriksaannifas.tanggal) = $this->year")
            ->get();

        return $nifas;
    }
}
