<?php

namespace App\Http\Controllers;

use App\Models\AnakModel;
use App\Models\AntrianModel;
use App\Models\BayiBLModel;
use App\Models\IbuBersalinModel;
use App\Models\IbuHamilModel;
use App\Models\KBModel;
use App\Models\NifasModel;
use App\Models\PasienModel;
use App\Models\PemeriksaanModel;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Pemeriksaanbersalin;

class PasienController extends Controller
{
    //menampilkan data pasien
    public function index(Request $request)
    {

        $pasiens = PasienModel::leftJoin('users', 'pasiens.id_users', '=', 'users.id')
            ->select(DB::raw('pasiens.*, users.email,  TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun'))
            ->where('nama', 'like', "%" . $request->keyword . "%")
            ->get();
        return view("admin/pasien", ["pasien" => $pasiens]);
    }

    //menyimpan data anak
    public function storeAnak(Request $request)
    {
        try {
            /* variabel form all */
            $nama = $request->nama;
            $jk = $request->jk;
            $id_orangtua = Auth::id();
            $get = PasienModel::where('id_users', $id_orangtua)->first();
            $alamat = isset($get->alamat) ? $get->alamat : null;
            $tanggal_lahir = date('Y-m-d', strtotime($request->tanggal_lahir));
            $id_users = null;
            $pasien =
                [
                    'nama' => $nama,
                    'jk' => $jk,
                    'alamat' => $alamat,
                    'tanggal_lahir' => $tanggal_lahir,
                    'id_orangtua' => $id_orangtua,
                    'id_users' => $id_users
                ];
            $pasien = PasienModel::create($pasien);
            return redirect()->route('dashboard_pasien');
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }

    //menampilkan detail riwayat berdasarkan id
    public function detail($id)
    {

        $ibuhamil = IbuHamilModel::join('pasiens', 'pemeriksaanibuhamil.id_pasien', '=', 'pasiens.id')
            ->where('pasiens.id', '=', $id)->select(DB::raw('pemeriksaanibuhamil.*, "ibu hamil" as jenis_periksa'))
            ->get();
        $ibubersalin = IbuBersalinModel::join('pasiens', 'pemeriksaanbersalin.id_pasien', '=', 'pasiens.id')
            ->where('pasiens.id', '=', $id)->select(DB::raw('pemeriksaanbersalin.*, "ibu hamil" as jenis_periksa'))
            ->get();
        $nifas = NifasModel::join('pasiens', 'pemeriksaannifas.id_pasien', '=', 'pasiens.id')
            ->where('pasiens.id', '=', $id)->select(DB::raw('pemeriksaannifas.*, "ibu hamil" as jenis_periksa'))
            ->get();
        $kb = KBModel::join('pasiens', 'pemeriksaankb.id_pasien', '=', 'pasiens.id')
            ->where('pasiens.id', '=', $id)->select(DB::raw('pemeriksaankb.*, "ibu hamil" as jenis_periksa'))
            ->get();
        $pemeriksaan_anak = AnakModel::join('pasiens', 'pemeriksaananak.id_pasien', '=', 'pasiens.id')
            ->where('pasiens.id', '=', $id)->select(DB::raw('pemeriksaananak.*, "ibu hamil" as jenis_periksa'))
            ->get();
        $bayibarulahir = BayiBLModel::join('pasiens', 'pemeriksaanbayibl.id_pasien', '=', 'pasiens.id')
            ->where('pasiens.id', '=', $id)->select(DB::raw('pemeriksaanbayibl.*, "ibu hamil" as jenis_periksa'))
            ->get();
        return view('admin/modal/detailpasien', ['ibuhamil' => $ibuhamil, 'ibubersalin' => $ibubersalin, 'nifas' => $nifas, 'kb' => $kb, 'pemeriksaan_anak' => $pemeriksaan_anak, 'bayibarulahir' => $bayibarulahir]);
    }
}
