<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Exports\KBExport;
use App\Models\AntrianModel;
use App\Models\KBModel;
use App\Models\PasienModel;
use Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class KBController extends Controller
{
    //
    public function index(Request $request)
    {

        $kb = KBModel::join('pasiens', 'pemeriksaankb.id_pasien', '=', 'pasiens.id')
            ->where('nama', 'like', "%" . $request->keyword . "%")
            ->get(['pemeriksaankb.*', 'pasiens.nama', 'pasiens.alamat']);

        return view("admin/kb", ["kb" => $kb]);
    }

    public function store(Request $request)
    {

        try {
            /* variabel form all */
            // $id_pasien = PasienModel::where('id', '=', session::get('id'))->select('id')->first();
            $id_pasien = $request->id_pasien;

            $id_antrian = $request->id_antrian;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $berat_badan = $request->berat_badan;
            $tekanan_darah = $request->tekanan_darah;
            $akseptor_kb = $request->akseptor_kb;
            $tx = $request->tx;
            $kie = $request->kie;
            // print_r($berat_badan);
            // exit;

            $kb = KBModel::create(
                [
                    'id_pasien' => $id_pasien,
                    'tanggal' => $tanggal,
                    'subject' => $subject,
                    'berat_badan' => $berat_badan,
                    'tekanan_darah' => $tekanan_darah,
                    'akseptor_kb' => $akseptor_kb,
                    'tx' => $tx,
                    'kie' => $kie
                ]
            );
            $antrian = AntrianModel::where('id', $id_antrian)->update(
                [
                    'status' => 'sudah diperiksa'
                ]
            );
            return redirect('/antrian');

            // $kb = KBModel::all();
            // return redirect('/laporan/kb')->with('kb', $kb);
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }



    public function tambahkb($id, $id_antrian)
    {
        $kb = PasienModel::where('pasiens.id', $id)
            ->select(['pasiens.*', 'pasiens.nama'])
            ->first();


        return view('admin/tambahkb', ["kb" => $kb, "id_antrian" => $id_antrian]);
    }

    public function update($id)
    {
        $kb = KBModel::join('pasiens', 'pemeriksaankb.id_pasien', '=', 'pasiens.id')
            ->where('pemeriksaankb.id', $id)
            ->select(['pemeriksaankb.*', 'pasiens.nama'])
            ->first();

        // $jadwal = JadwalModel::find($id);
        // print_r($jadwals);
        // exit;
        return view('admin/editkb', ['kb' => $kb]);
    }

    public function editkb(Request $request)
    {

        try {
            /* variabel form all */
            $id = $request->id;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $berat_badan = $request->berat_badan;
            $tekanan_darah = $request->tekanan_darah;
            $akseptor_kb = $request->akseptor_kb;
            $tx = $request->tx;
            $kie = $request->kie;


            $kb = KBModel::find($id);

            $kb->id = $id;
            $kb->tanggal = $tanggal;
            $kb->subject = $subject;
            $kb->berat_badan = $berat_badan;
            $kb->tekanan_darah = $tekanan_darah;
            $kb->akseptor_kb = $akseptor_kb;
            $kb->tx = $tx;
            $kb->kie = $kie;

            $kb->save();
            $kb = KBModel::all();
            return redirect('/laporan/kb')->with('kb', $kb);
        } catch (Exception $error) {
            return "Gagal Diubah";
        }
    }


    public function delete($id)
    {
        $kb = KBModel::find($id);
        $kb->link = "/laporan/kb/destroy/$id";
        return view('admin.modal.modalkb', ['kb' => $kb]);
    }
    public function destroy($id)
    {
        $kb = KBModel::find($id);
        $kb->delete();
        $kb = KBModel::all();
        return redirect('/laporan/kb')->with('kb', $kb);
    }

    public function export(Request $request)
    {
        $tanggal = isset($request->tahun) ? $request->tahun : date('Y');
        return Excel::download(new KBExport($tanggal), 'kbexport.xlsx');
    }
}
