<?php

namespace App\Http\Controllers;

use App\Exports\BayiBLExport;
use App\Models\BayiBLModel;
use App\Models\PasienModel;
use Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BayiBLController extends Controller
{
    //
    public function index(Request $request)
    {

        $bayibl = BayiBLModel::join('pasiens', 'pemeriksaanbayibl.id_pasien', '=', 'pasiens.id')
            ->where('pasiens.nama', 'like', "%" . $request->keyword . "%")
            ->get(['pemeriksaanbayibl.*', 'pasiens.nama', 'pasiens.alamat']);

        return view("admin/bayibl", ["bayibl" => $bayibl]);
    }

    public function store(Request $request)
    {

        try {
            /* variabel form all */
            $id_pasien = $request->id_pasien;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $as = $request->as;
            $berat_badan = $request->berat_badan;
            $pb = $request->pb;
            $suhu = $request->suhu;
            $lika = $request->lika;
            $lila = $request->lila;
            $anus = $request->anus;
            $ku = $request->ku;
            $status_gizi = $request->status_gizi;
            $sdidtk = $request->sdidtk;
            $mtbm = $request->mtbm;
            $mtbs = $request->mtbs;
            $imunisasi = $request->imunisasi;
            $tx = $request->tx;
            $kie = $request->kie;
            // print_r($berat_badan);
            // exit;

            $bayibl = BayiBLModel::create(
                [
                    'id_pasien' => $id_pasien,
                    'tanggal' => $tanggal,
                    'subject' => $subject,
                    'as' => $as,
                    'berat_badan' => $berat_badan,
                    'pb' => $pb,
                    'suhu' => $suhu,
                    'lika' => $lika,
                    'lila' => $lila,
                    'anus' => $anus,
                    'ku' => $ku,
                    'status_gizi' => $status_gizi,
                    'sdidtk' => $sdidtk,
                    'mtbm' => $mtbm,
                    'mtbs' => $mtbs,
                    'imunisasi' => $imunisasi,
                    'tx' => $tx,
                    'kie' => $kie
                ]
            );


            $bayibl = BayiBLModel::all();
            return redirect('/laporan/bayibl')->with('bayibl', $bayibl);
            // $antrian = AntrianModel::where("id_pasien", $id_pasien)->first();
            // $antrian->status = "Selesai";
            // $antrian->save();
            // $antrian = AntrianModel::all();
            // return redirect('/antrian')->with('antrian', $antrian);
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }



    public function tambahbayibl()
    {
        $pasiens = PasienModel::all();
        return view('admin/tambahbayibl', ["pasien" => $pasiens]);
    }

    public function update($id)
    {
        $bayibl = BayiBLModel::join('pasiens', 'pemeriksaanbayibl.id_pasien', '=', 'pasiens.id')
            ->where('pemeriksaanbayibl.id', $id)
            ->select(['pemeriksaanbayibl.*', 'pasiens.nama'])
            ->first();

        // $jadwal = JadwalModel::find($id);
        // print_r($jadwals);
        // exit;
        return view('admin/editbayibl', ['bayibl' => $bayibl]);
    }

    public function editbayibl(Request $request)
    {

        try {
            /* variabel form all */
            $id = $request->id;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $as = $request->as;
            $berat_badan = $request->berat_badan;
            $pb = $request->pb;
            $suhu = $request->suhu;
            $lika = $request->lika;
            $lila = $request->lila;
            $anus = $request->anus;
            $ku = $request->ku;
            $status_gizi = $request->status_gizi;
            $sdidtk = $request->sdidtk;
            $mtbm = $request->mtbm;
            $mtbs = $request->mtbs;
            $imunisasi = $request->imunisasi;
            $tx = $request->tx;
            $kie = $request->kie;


            $bayibl = BayiBLModel::find($id);

            $bayibl->id = $id;
            $bayibl->tanggal = $tanggal;
            $bayibl->subject = $subject;
            $bayibl->as = $as;
            $bayibl->berat_badan = $berat_badan;
            $bayibl->pb = $pb;
            $bayibl->suhu = $suhu;
            $bayibl->lika = $lika;
            $bayibl->lila = $lila;
            $bayibl->anus = $anus;
            $bayibl->ku = $ku;
            $bayibl->status_gizi = $status_gizi;
            $bayibl->sdidtk = $sdidtk;
            $bayibl->mtbm = $mtbm;
            $bayibl->mtbs = $mtbs;
            $bayibl->imunisasi = $imunisasi;
            $bayibl->tx = $tx;
            $bayibl->kie = $kie;

            $bayibl->save();
            $bayibl = BayiBLModel::all();
            return redirect('/laporan/bayibl')->with('bayibl', $bayibl);
        } catch (Exception $error) {
            return "Gagal Diubah";
        }
    }


    public function delete($id)
    {
        $bayibl = BayiBLModel::find($id);
        $bayibl->link = "/laporan/bayibl/destroy/$id";
        return view('admin.modal.modalbayibl', ['bayibl' => $bayibl]);
    }
    public function destroy($id)
    {
        $bayibl = BayiBLModel::find($id);
        $bayibl->delete();
        $bayibl = BayiBLModel::all();
        return redirect('/laporan/bayibl')->with('bayibl', $bayibl);
    }
    public function export(Request $request)
    {
        $tanggal = isset($request->tahun) ? $request->tahun : date('Y');
        return Excel::download(new BayiBLExport($tanggal), 'bayiblexport.xlsx');
    }
}
