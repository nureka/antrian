<?php

namespace App\Http\Controllers;

use App\Exports\IbuHamilExport;
use App\Models\AntrianModel;
use App\Models\IbuHamilModel;
use App\Models\PasienModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class IbuHamilController extends Controller
{
    //
    public function index(Request $request)
    {
        // print_r($request->tanggal);exit;
        $tanggal = isset($request->tahun) ? $request->tahun : date('Y');

        $ibuhamil = IbuHamilModel::join('pasiens', 'pemeriksaanibuhamil.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaanibuhamil.*, pasiens.nama, pasiens.alamat,  TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun'))
            ->whereRaw("YEAR(pemeriksaanibuhamil.tanggal) = $tanggal")
            ->where('nama', 'like', "%" . $request->keyword . "%")
            
            ->get();

        return view("admin/ibuhamil", ["ibuhamil" => $ibuhamil]);
    }

    public function store(Request $request)
    {

        try {
            /* variabel form all */
            $id_pasien = $request->id_pasien;
            $id_antrian = $request->id_antrian;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $berat_badan = $request->berat_badan;
            $lila = $request->lila;
            $tekanan_darah = $request->tekanan_darah;
            $nadi = $request->nadi;
            $suhu = $request->suhu;
            $tinggi_fundus_uteri = $request->tinggi_fundus_uteri;
            $denyut_jantung = $request->denyut_jantung;
            $let = $request->let;
            $lab = $request->lab;
            $skor = $request->skor;
            $g = $request->g;
            $tx = $request->tx;
            $kie = $request->kie;
            // print_r($id_pasien);
            // exit;

            $ibuhamil = IbuHamilModel::create(
                [
                    'id_pasien' => $id_pasien,
                    'tanggal' => $tanggal,
                    'subject' => $subject,
                    'berat_badan' => $berat_badan,
                    'lila' => $lila,
                    'tekanan_darah' => $tekanan_darah,
                    'nadi' => $nadi,
                    'suhu' => $suhu,
                    'tinggi_fundus_uteri' => $tinggi_fundus_uteri,
                    'denyut_jantung' => $denyut_jantung,
                    'let' => $let,
                    'lab' => $lab,
                    'skor' => $skor,
                    'g' => $g,
                    'tx' => $tx,
                    'kie' => $kie
                ]
            );

            $antrian = AntrianModel::where('id', $id_antrian)->update(
                [
                    'status' => 'sudah diperiksa'
                ]
            );



            return redirect('/antrian');
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }



    public function tambahibuhamil($id, $id_antrian)
    {

        $ibuhamil = PasienModel::where('pasiens.id', $id)
            ->select(['pasiens.*', 'pasiens.nama'])
            ->first();

        // print_r($ibuhamil);
        // exit;
        return view('admin/tambahibuhamil', ["ibuhamil" => $ibuhamil, "id_antrian" => $id_antrian]);
    }

    public function update($id)
    {
        $ibuhamil = IbuHamilModel::join('pasiens', 'pemeriksaanibuhamil.id_pasien', '=', 'pasiens.id')
            ->where('pemeriksaanibuhamil.id', $id)
            ->select(['pemeriksaanibuhamil.*', 'pasiens.nama'])
            ->first();

        // $jadwal = JadwalModel::find($id);
        // print_r($jadwals);
        // exit;
        return view('admin/editibuhamil', ['ibuhamil' => $ibuhamil]);
    }

    public function editibuhamil(Request $request)
    {

        try {
            /* variabel form all */
            $id = $request->id;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $berat_badan = $request->berat_badan;
            $lila = $request->lila;
            $tekanan_darah = $request->tekanan_darah;
            $nadi = $request->nadi;
            $suhu = $request->suhu;
            $tinggi_fundus_uteri = $request->tinggi_fundus_uteri;
            $denyut_jantung = $request->denyut_jantung;
            $let = $request->let;
            $lab = $request->lab;
            $skor = $request->skor;
            $g = $request->g;
            $tx = $request->tx;
            $kie = $request->kie;


            $ibuhamil = IbuHamilModel::find($id);

            $ibuhamil->id = $id;
            $ibuhamil->tanggal = $tanggal;
            $ibuhamil->subject = $subject;
            $ibuhamil->berat_badan = $berat_badan;
            $ibuhamil->lila = $lila;
            $ibuhamil->tekanan_darah = $tekanan_darah;
            $ibuhamil->nadi = $nadi;
            $ibuhamil->suhu = $suhu;
            $ibuhamil->tinggi_fundus_uteri = $tinggi_fundus_uteri;
            $ibuhamil->denyut_jantung = $denyut_jantung;
            $ibuhamil->let = $let;
            $ibuhamil->lab = $lab;
            $ibuhamil->skor = $skor;
            $ibuhamil->g = $g;
            $ibuhamil->tx = $tx;
            $ibuhamil->kie = $kie;

            $ibuhamil->save();
            $ibuhamil = IbuHamilModel::all();
            return redirect('/laporan/ibuhamil')->with('ibuhamil', $ibuhamil);
        } catch (Exception $error) {
            return "Gagal Diubah";
        }
    }


    public function delete($id)
    {
        $ibuhamil = IbuHamilModel::find($id);
        $ibuhamil->link = "/laporan/ibuhamil/destroy/$id";
        return view('admin.modal.modalibuhamil', ['ibuhamil' => $ibuhamil]);
    }
    public function destroy($id)
    {
        $ibuhamil = IbuHamilModel::find($id);
        $ibuhamil->delete();
        $ibuhamil = IbuHamilModel::all();
        return redirect('/laporan/ibuhamil')->with('ibuhamil', $ibuhamil);
    }

    public function export(Request $request)
    {
        $tanggal = isset($request->tahun) ? $request->tahun : date('Y');
        return Excel::download(new IbuHamilExport($tanggal), 'ibuhamilexport.xlsx');
    }
}
