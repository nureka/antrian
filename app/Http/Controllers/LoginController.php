<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Redirect;
use App\Models\JadwalModel;
use App\Models\LoginModel;
use App\Models\PasienModel;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    //menampilkan data pasien dan anak pada dashboard pasien
    public function index()
    {

        $id = Auth::id();
        if ($id) {
            $data_pasien = PasienModel::where('id_users', $id)->select(DB::raw('pasiens.*, 
        TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun, 
        TIMESTAMPDIFF( MONTH, pasiens.tanggal_lahir, now() ) % 12 as bulan, 
        FLOOR( TIMESTAMPDIFF( DAY, pasiens.tanggal_lahir, now() ) % 30.4375 ) as hari'))->first();

            $data_anak = PasienModel::where('pasiens.id_orangtua', $id)->select(DB::raw('pasiens.*, 
        TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun, 
        TIMESTAMPDIFF( MONTH, pasiens.tanggal_lahir, now() ) % 12 as bulan, 
        FLOOR( TIMESTAMPDIFF( DAY, pasiens.tanggal_lahir, now() ) % 30.4375 ) as hari'))->get();

            return view('dashboard')->with('data_pasien', $data_pasien)->with('data_anak', $data_anak);
        } else {
            return view('login');
        }
    }

    //cek data login
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        $email = $request->email;
        $password = $request->password;
        try {
            /* cek login */
            $credentials = request(['email', 'password']);
            $email = $request->email;
            $password = $request->password;

            $user = LoginModel::where('email', $request->email)->first();
            $id = $user->id;
            if ($request->password != $user->password) {
                return view('login')->with('data_', "Email atau Password Salah");
            } else {
                $request->session()->regenerate();
            
                if ($user->type == 'admin') {
                    return view('admin/dashboard');
                    session(['id' => $id]);
                } else {
                    session(['id' => $id]);
                    return $this->index();
                }
            }
        } catch (Exception $error) {
            return view('login')->with('data_', "Email atau Password Salah");
        }
    }

    //menghapus session user
    public function logout()
    {
        session()->flush();
        session()->invalidate();
 
        return redirect('/');
    }



    
    public function dologin(Request $request){
 		

        $this->validate($request,[
            'email' => 'required',
            'password' => 'required|string'
        ]);


        if (Auth::attempt(["email" => $request->email, "password" => $request->password]) ) {
            // return view('admin/dashboard');
           
            if(Auth::user()->type == 'admin'){
                return view('admin/dashboard');
            }else{
                return $this->index();
            }
        }else{
            return view('login')->with('data_', "Email atau Password Salah");
        }
    }
}
