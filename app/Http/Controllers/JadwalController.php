<?php

namespace App\Http\Controllers;

use App\Models\AntrianModel;
use App\Models\BayiBLModel;
use App\Models\JadwalModel;
use App\Models\PasienModel;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Google\Client;
use Google\Service\Calendar;
use Google\Service\Calendar\Event;
use Google\Service\Calendar\EventDateTime;
use Google\Service\Calendar\EventReminder;
use Google\Service\Calendar\EventReminders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class JadwalController extends Controller
{

    public function index(Request $request)
    {
        //menampilkan data 
        //nama alamat tgl lahir join pasien
        $jadwals = JadwalModel::join('pasiens', 'jadwals.id_pasien', '=', 'pasiens.id')
            ->where('nama', 'like', "%" . $request->keyword . "%")
            ->get(['jadwals.*', 'pasiens.nama']);

        return view("admin/jadwal", ["jadwal" => $jadwals]);
    }

    //mengambil data pasien
    public function tambahjadwal()
    {

        $pasiens = PasienModel::all();
        return view('admin/tambahjadwal', ["pasien" => $pasiens]);
    }

    //menyimpan jadwal
    public function store(Request $request)
    {
        try {
            /* variabel form all */
            $id_pasien = $request->id_pasien;
            $tanggal = $request->tanggal;
            $jam = $request->jam;
            $jenisperiksa = $request->jenisperiksa;
            $keterangan = $request->keterangan;

            $user = User::join('pasiens', 'pasiens.id_users', '=', 'users.id')
                ->where('pasiens.id', $request->id_pasien)
                ->first('users.*');

            $jadwal = JadwalModel::create(
                [
                    'id_pasien' => $id_pasien,
                    'tanggal' => $tanggal,
                    'jam' => $jam,
                    'jenisperiksa' => $jenisperiksa,
                    'keterangan' => $keterangan,
                ]
            );
            //start event
            $start = Carbon::createFromFormat('Y-m-dH:i', $tanggal . $jam, 'Asia/Jakarta')->toISOString(true);
            //end event
            $end = Carbon::createFromFormat('Y-m-dH:i', $tanggal . $jam, 'Asia/Jakarta')->addHours(2)->toISOString(true);
            //peserta
            $attendees = array(
                array('email' => $user->email),
            );

            $reminders = array(
                'useDefault' => false,
                'overrides' => array(
                    array('method' => 'email', 'minutes' => 1440), // h-1 before event
                    array('method' => 'popup', 'minutes' => 1440), // h-1 before event
                ),
            );
            [$error, $event] = $this->insertCalendarEvent(array(
                'summary' => 'Jadwal Kontrol - ' . $jenisperiksa,
                'description' => $keterangan,
                'start' => array(
                    'dateTime' => $start,
                    'timeZone' => 'Asia/Jakarta',
                ),
                'end' => array(
                    'dateTime' => $end,
                    'timeZone' => 'Asia/Jakarta',
                ),
                'attendees' => $attendees,
                'reminders' => $reminders,
            ));
            if ($error) {
                $jadwal->delete();
                throw $error;
            }

            $jadwal->event_id = $event->id;
            $jadwal->save();

            $jadwal = JadwalModel::all();
            return redirect('/jadwal')->with('jadwal', $jadwal);
        } catch (Exception $error) {
            Log::error($error->getMessage());
            return "Gagal Disimpan";
        }
    }

    public function update($id)
    {
        $jadwals = JadwalModel::join('pasiens', 'jadwals.id_pasien', '=', 'pasiens.id')
            ->where('jadwals.id', $id)
            ->select(['jadwals.*', 'pasiens.nama'])
            ->first();

        // $jadwal = JadwalModel::find($id);
        // print_r($jadwals);
        // exit;
        return view('admin/editjadwal', ['jadwal' => $jadwals]);
    }

    public function editjadwal(Request $request)
    {

        try {
            /* variabel form all */
            $id = $request->id;
            $tanggal = $request->tanggal;
            $jam = $request->jam;
            $jenisperiksa = $request->jenisperiksa;
            $keterangan = $request->keterangan;

            $jadwal = JadwalModel::find($id);

            $jadwal->id = $id;
            $jadwal->tanggal = $tanggal;
            $jadwal->jam = $jam;
            $jadwal->jenisperiksa = $jenisperiksa;
            $jadwal->keterangan = $keterangan;

            $user = User::join('pasiens', 'pasiens.id_users', '=', 'users.id')
                ->where('pasiens.id', $jadwal->id_pasien)
                ->first('users.*');

            $start = Carbon::createFromFormat('Y-m-d' . (count(explode(':', $jam)) === 2 ? 'H:i' : 'H:i:s'), $tanggal . $jam, 'Asia/Jakarta')->toISOString(true);
            $end = Carbon::createFromFormat('Y-m-d' . (count(explode(':', $jam)) === 2 ? 'H:i' : 'H:i:s'), $tanggal . $jam, 'Asia/Jakarta')->addHours(2)->toISOString(true);
            $attendees = array(
                array('email' => $user->email),
            );
            $reminders = array(
                'useDefault' => false,
                'overrides' => array(
                    array('method' => 'email', 'minutes' => 1440), // h-1 before event
                    array('method' => 'popup', 'minutes' => 1440), // h-1 minutes before event
                ),
            );
            [$error, $event] = $this->updateCalendarEvent(
                $jadwal->event_id,
                array(
                    'summary' => 'Jadwal Kontrol - ' . $jenisperiksa,
                    'description' => $keterangan,
                    'start' => array(
                        'dateTime' => $start,
                        'timeZone' => 'Asia/Jakarta',
                    ),
                    'end' => array(
                        'dateTime' => $end,
                        'timeZone' => 'Asia/Jakarta',
                    ),
                    'attendees' => $attendees,
                    'reminders' => $reminders,
                )
            );
            if ($error) {
                throw $error;
            }

            $jadwal->save();
            $jadwal = JadwalModel::all();
            return redirect('/jadwal')->with('jadwal', $jadwal);
        } catch (Exception $error) {
            Log::error($error->getMessage());
            return "Gagal Diubah";
        }
    }

    public function delete($id)
    {
        $jadwals = JadwalModel::find($id);
        $jadwals->link = "/jadwal/destroy/$id";
        return view('admin.modal.modalJadwal', ['jadwal' => $jadwals]);
    }
    public function destroy($id)
    {
        $jadwals = JadwalModel::find($id);
        [$error] = $this->deleteCalendarEvent(
            $jadwals->event_id,
        );
        if ($error) {
            throw $error;
        }

        $jadwals->delete();
        $jadwals = JadwalModel::all();
        return redirect('/jadwal')->with('jadwal', $jadwals);
    }

    //mengambil data pasien
    public function jadwal()
    {
        $id = Auth::id();

        $pasien = PasienModel::where('id_users', $id)->first();
        $jadwals = JadwalModel::where('id_pasien', $pasien->id)->get();

        return view('calendar.index', ['jadwals' => $jadwals]);
    }

    //mengambil data jadwal 
    public function jadwalKontrol()
    {
        $id = Auth::id();
        // print_r($id);exit;
        $pasien = PasienModel::where('id_users', $id)->first();
        $jadwals = JadwalModel::join('pasiens', 'jadwals.id_pasien', '=', 'pasiens.id')
            ->where('jadwals.id_pasien', $pasien->id)
            ->select(DB::raw('jadwals.jenisperiksa as title, jadwals.keterangan as description,
                    jadwals.tanggal as start, "calendar" as icon ,"fc-bg-default" as className,pasiens.nama'))
            ->get();
        //mengubah array ke json
        return response()->json(array('success' => true, 'data' => $jadwals));
    }
    //menampilkan data bulan ini+5bulan kedepan
    public function dataDashboard()
    {

        // print_r(date('m', strtotime('+5 month')));exit;
        $bulan_ini = date('m');
        $bulan_maks = date('m', strtotime('+5 month'));
        // $pasien = PasienModel::where('id_users', $id)->first();
        $antrian = AntrianModel::join('pasiens', 'antrians.id_pasien', '=', 'pasiens.id')
            ->whereNotNull('pasiens.id_users')
            ->whereRaw("MONTH(antrians.tanggal) >= 01")
            ->whereRaw("MONTH(antrians.tanggal) <=  12")
            ->groupByRaw('MONTH(antrians.tanggal)')
            ->select(DB::raw('count(antrians.id) as jumlah, MONTH(antrians.tanggal) as bulan'))
            ->get();
        $antrian_anak = AntrianModel::join('pasiens', 'antrians.id_pasien', '=', 'pasiens.id')
            ->whereNull('pasiens.id_users')
            ->whereRaw("MONTH(antrians.tanggal) >= 01")
            ->whereRaw("MONTH(antrians.tanggal) <=  12")
            ->groupByRaw('MONTH(antrians.tanggal)')
            ->select(DB::raw('count(antrians.id) as jumlah, MONTH(antrians.tanggal) as bulan'))
            ->get();

        // if (!empty($antrian_anak[2]) && isset($antrian_anak[2])) {
        //     print($antrian_anak);
        // }else{
        //     print('bbbb');
        // }
        // exit;

        //data diolah menjadi array dibedakan antara jumlah kunjungan ibu dan anak
        $data_baru = [];
        //jika bulan = bulan ini maka jumlah kunjungan masuk dibulan ini jika tidak 0
        for ($i = 0; $i < 7; $i++) {
            $bulan_maks = date('F', strtotime("+$i month"));
            if (!empty($antrian[$i]) && isset($antrian[$i])) {
                if ($antrian[$i]->bulan == $bulan_ini) {
                    $data_baru[$i]['ibu'] = $antrian[$i]->jumlah;
                } else {
                    $data_baru[$i]['ibu'] = 0;
                }
            } else {
                $data_baru[$i]['ibu'] = 0;
            }

            if (!empty($antrian_anak[$i]) && isset($antrian_anak[$i])) {
                if (isset($antrian_anak) && !empty($antrian_anak)) {
                    if ($antrian_anak[$i]->bulan == $bulan_ini) {
                        $data_baru[$i]['anak'] = $antrian_anak[$i]->jumlah;
                    } else {
                        $data_baru[$i]['anak'] = 0;
                    }
                } else {
                    $data_baru[$i]['anak'] = 0;
                }
            } else {
                $data_baru[$i]['anak'] = 0;
            }
            $data_baru[$i]['bulan'] = $bulan_maks;
        }
        // print_r($data_baru);
        // exit;

        return response()->json(array('success' => true, 'data' => $data_baru));
    }

    //menampilkan data bulan ini+5bulan kedepan
    public function dataDashboard_kelahiran()
    {

        // print_r(date('m', strtotime('+5 month')));exit;
        $tahun_ini = date('Y');
        $tahun_maks = date('Y', strtotime('+5 year'));
        // $pasien = PasienModel::where('id_users', $id)->first();
        $antrian = BayiBLModel::whereRaw("YEAR(tanggal) >= $tahun_ini")
            ->whereRaw("YEAR(tanggal) <=  $tahun_maks")
            ->groupByRaw('YEAR(pemeriksaanbayibl.tanggal)')
            ->select(DB::raw('count(pemeriksaanbayibl.id) as jumlah, YEAR(pemeriksaanbayibl.tanggal) as tahun'))
            ->get();
        // $data_baru = [];
        for ($i = 0; $i < 7; $i++) {
            $tahun_maks = date('Y', strtotime("+$i year"));
            if (!empty($antrian[$i]) && isset($antrian[$i])) {
                if ($antrian[$i]->tahun == $tahun_ini) {
                    $data_baru[$i]['kelahiran'] = $antrian[$i]->jumlah;
                } else {
                    $data_baru[$i]['kelahiran'] = 0;
                }
            } else {
                $data_baru[$i]['kelahiran'] = 0;
            }

            $data_baru[$i]['tahun'] = $tahun_maks;
        }
        // print_r($data_baru);
        // exit;

        return response()->json(array('success' => true, 'data' => $data_baru));
    }

    /**
     * Function to insert new event to users calendar by using google apis
     *
     * @param array $payload
     * @return array
     *
     * @internal $event can contains value as mentions here. https: //developers.google.com/calendar/api/v3/reference/events/insert#request-body
     * @internal return from this function will be an array like this, [Error || null, Event || null]
     */

    //menambah event
    private function insertCalendarEvent($payload)
    {
        try {
            $client = $this->getClient();
            $service = new Calendar($client);
            $event = new Event($payload);

            $calendarId = 'primary'; // default calendarId for each user is primary
            $event = $service->events->insert($calendarId, $event, array(
                'sendNotifications' => true,
                'sendUpdates' => 'all',
            ));

            return [null, $event];
        } catch (\Exception $e) {
            return [$e, null];
        }
    }

    /**
     * Function to update an existing event to users calendar by using google apis
     *
     * @param string $id
     * @param array $payload
     * @return array
     *
     * @internal $event can contains value as mentions here. https: //developers.google.com/calendar/api/v3/reference/events/insert#request-body
     * @internal return from this function will be an array like this, [Error || null, Event || null]
     */
    private function updateCalendarEvent($id, $payload)
    {
        try {
            $client = $this->getClient();
            $service = new Calendar($client);
            $calendarId = 'primary'; // default calendarId for each user is primary

            $event = $service->events->get($calendarId, $id);
            if ($payload['start']) {
                $start = new EventDateTime();
                $start->setDateTime($payload['start']['dateTime']);
                $start->setTimeZone($payload['start']['timeZone']);

                $event->setStart($start ?? $event->getStart());
            }
            if ($payload['end']) {
                $end = new EventDateTime();
                $end->setDateTime($payload['end']['dateTime']);
                $end->setTimeZone($payload['end']['timeZone']);

                $event->setEnd($end ?? $event->getEnd());
            }
            if ($payload['reminders']) {
                $reminders = new EventReminders();
                $reminders->setUseDefault(false);

                $overrides = [];
                foreach ($payload['reminders']['overrides'] as $reminder) {
                    $override = new EventReminder();
                    $override->setMethod($reminder['method']);
                    $override->setMinutes($reminder['minutes']);
                    $overrides = [$override];
                }
                $reminders->setOverrides($overrides);
                $event->setReminders($reminders ?? $event->getReminders());
            }

            $event->setSummary($payload['summary'] ?? $event->getSummary());
            $event->setDescription($payload['description'] ?? $event->getDescription());
            $event->setAttendees($payload['attendees'] ?? $event->getAttendees());

            $event = $service->events->update($calendarId, $id, $event, array(
                'sendNotifications' => true,
                'sendUpdates' => 'all',
            ));

            return [null, $event];
        } catch (\Exception $e) {
            return [$e, null];
        }
    }

    /**
     * Function to delete an existing event to users calendar by using google apis
     *
     * @param string $id
     * @return array
     *
     * @internal $event can contains value as mentions here. https: //developers.google.com/calendar/api/v3/reference/events/insert#request-body
     * @internal return from this function will be an array like this, [Error || null, Event || null]
     */
    private function deleteCalendarEvent($id)
    {
        try {
            $client = $this->getClient();
            $service = new Calendar($client);
            $calendarId = 'primary'; // default calendarId for each user is primary

            $service->events->delete($calendarId, $id);

            return [null];
        } catch (\Exception $e) {
            return [$e];
        }
    }

    /**
     * Function to get google client
     *
     * @return Client
     */

    //otorisasi aplikasi ke google
    private function getClient()
    {
        $client = new Client();
        $client->setApplicationName('antrian');
        $client->setScopes(Calendar::CALENDAR);
        $client->setAuthConfig(__DIR__ . '/../../../credentials.json');
        $tokenPath = __DIR__ . '/../../../token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }
        //ngecek token expired
        if ($client->isAccessTokenExpired()) {
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }

        return $client;
    }
}
