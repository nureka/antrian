<?php

namespace App\Http\Controllers;

use App\Models\AntrianModel;
use App\Models\PasienModel;
use App\Models\PemeriksaanModel;
use Exception;
use Illuminate\Http\Request;

class PemeriksaanController extends Controller
{

    public function index()
    {
        $pemeriksaans = PemeriksaanModel::all();
        return view("admin/pemeriksaan", ["pemeriksaan" => $pemeriksaans]);
    }
    public function show($id)
    {
        $pemeriksaans = PemeriksaanModel::where('id_pasien', $id)->get();
        // return view("admin/pemeriksaan", ["pemeriksaan" => $pemeriksaans]);
        // return view("admin/pemeriksaan", ["pemeriksaan" => $pemeriksaans]);
        // $pasiens = PasienModel::all();
        return view("admin/pemeriksaan", ["pemeriksaan" => $pemeriksaans]);
    }

    public function store(Request $request)
    {

        try {
            /* variabel form all */
            $id_pasien = $request->id_pasien;
            $tanggal = date("Y-m-d");
            $keadaan_umum = $request->keadaan_umum;
            $berat_badan = $request->berat_badan;
            $tekanan_darah = $request->tekanan_darah;
            $suhu = $request->suhu;
            $nadi = $request->nadi;
            $denyut_jantung = $request->denyut_jantung;
            $tinggi_fundus_uteri = $request->tinggi_fundus_uteri;

            // print_r($berat_badan);
            // exit;

            $pemeriksaan = PemeriksaanModel::create(
                [
                    'id_pasien' => $id_pasien,
                    'tanggal' => $tanggal,
                    'keadaan_umum' => $keadaan_umum,
                    'berat_badan' => $berat_badan,
                    'tekanan_darah' => $tekanan_darah,
                    'suhu' => $suhu,
                    'nadi' => $nadi,
                    'denyut_jantung' => $denyut_jantung,
                    'tinggi_fundus_uteri' => $tinggi_fundus_uteri
                ]
            );



            $antrian = AntrianModel::where("id_pasien", $id_pasien)->first();
            $antrian->status = "Selesai";
            $antrian->save();
            $antrian = AntrianModel::all();
            return redirect('/antrian')->with('antrian', $antrian);
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }

    public function tambahpemeriksaan($id)
    {

        return view('admin/tambahpemeriksaan')->with("id_pasien", $id);
    }

    public function update($id)
    {

        $pemeriksaan = PemeriksaanModel::find($id);
        // $jadwal = JadwalModel::find($id);
        // print_r($jadwals);
        // exit;
        return view('admin/editpemeriksaan', ['pemeriksaan' => $pemeriksaan]);
    }

    public function editpemeriksaan(Request $request)
    {

        try {
            /* variabel form all */
            $id = $request->id;
            $tanggal = $request->tanggal;
            $keadaan_umum = $request->keadaan_umum;
            $berat_badan = $request->berat_badan;
            $suhu = $request->suhu;
            $nadi = $request->nadi;
            $denyut_jantung = $request->denyut_jantung;
            $tinggi_fundus_uteri = $request->tinggi_fundus_uteri;


            $pemeriksaan = PemeriksaanModel::find($id);

            $pemeriksaan->id = $id;
            $pemeriksaan->tanggal = $tanggal;
            $pemeriksaan->jam = $keadaan_umum;
            $pemeriksaan->berat_badan = $berat_badan;
            $pemeriksaan->suhu = $suhu;
            $pemeriksaan->nadi = $nadi;
            $pemeriksaan->denyut_jantung = $denyut_jantung;
            $pemeriksaan->tinggi_fundus_uteri = $tinggi_fundus_uteri;

            $pemeriksaan->save();
            $pemeriksaan = PemeriksaanModel::all();
            return redirect('/pemeriksaan')->with('pemeriksaan', $pemeriksaan);
        } catch (Exception $error) {
            return "Gagal Diubah";
        }
    }
}
