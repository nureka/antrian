<?php

namespace App\Http\Controllers;

use App\Models\LoginModel;
use App\Models\PasienModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{

    //menyimpan data regist
    public function register(Request $request)
    {

        //cek email
        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            return view('register')->with('data_', "Email sudah terdaftar");
        }
        try {
            /* variabel form all */
            $nama = $request->nama;
            $email = $request->email;
            $password = Hash::make($request['password']);
            $tanggal_lahir = $request->tanggal_lahir;
            $alamat = $request->alamat;

            /* variabel form orangtua */
            //simpan ke db login
            $user = LoginModel::create([
                'name' => $nama,
                'email' => $email,
                'password' => $password
            ]);

            //simpan ke db pasien
            $pasien = PasienModel::create(
                [
                    'nama' => $nama,
                    'tanggal_lahir' => $tanggal_lahir,
                    'alamat' => $alamat,
                    'id_users' => $user->id,
                    'id_orangtua' => null
                ]
            );


            return $this->login($user);
        } catch (Exception $error) {
            return view('register')->with('data_', "Email sudah terdaftar");
        }
    }

    //redirect login dg mengecek data login
    public function login($param)
    {

        $email = $param->email;
        $password = $param->password;
        try {
            /* cek login */

            $email = $param->email;
            $password = $param->password;

            $user = LoginModel::where('email', $param->email)->first();
            // print_r($user);
            // exit;
            $id = $user->id;
            // print_r($id);exit;
            if ($param->password != $user->password) {
                return view('login');
            } else {
                if ($user->type == 'admin') {
                    return view('admin/dashboard');
                    session(['id' => $id]);
                } else {
                    session(['id' => $id]);
                    session::save();
                    return redirect()->route('dashboard');
                }
            }
        } catch (Exception $error) {
            return view('login');
        }
    }
}
