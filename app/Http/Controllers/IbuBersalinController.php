<?php

namespace App\Http\Controllers;

use App\Exports\IbuBersalinExport;
use App\Models\IbuBersalinModel;
use App\Models\PasienModel;
use Illuminate\Support\Facades\Session;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class IbuBersalinController extends Controller
{
    //
    public function index(Request $request)
    {

        $ibubersalin = IbuBersalinModel::join('pasiens', 'pemeriksaanbersalin.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaanbersalin.*, pasiens.nama, pasiens.alamat,  TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun'))
            ->where('nama', 'like', "%" . $request->keyword . "%")
            ->get();

        return view("admin/ibubersalin", ["ibubersalin" => $ibubersalin]);
    }

    public function store(Request $request)
    {

        try {
            /* variabel form all */
            // $id_pasien = PasienModel::where('id_users', '=', Auth::id())->select('id')->first();
            $id_pasien = $request->id_pasien;
            $tanggal = date("Y-m-d");
            $partograf = $request->partograf;
            $kala_1 = $request->kala_1;
            $proses_persalinan = $request->proses_persalinan;
            $tekanan_darah = $request->tekanan_darah;
            $nadi = $request->nadi;
            $suhu = $request->suhu;
            $cu = $request->cu;
            $perineum = $request->perineum;
            $perdarahan = $request->perdarahan;
            $tx = $request->tx;
            $bbl = $request->bbl;
            $bb = $request->bb;
            $pb = $request->pb;
            $lika = $request->lika;
            $imd = $request->imd;
            // print_r($berat_badan);
            // exit;

            $ibubersalin = IbuBersalinModel::create(
                [
                    'id_pasien' => $id_pasien,
                    'tanggal' => $tanggal,
                    'partograf' => $partograf,
                    'kala_1' => $kala_1,
                    'proses_persalinan' => $proses_persalinan,
                    'tekanan_darah' => $tekanan_darah,
                    'nadi' => $nadi,
                    'suhu' => $suhu,
                    'cu' => $cu,
                    'perineum' => $perineum,
                    'perdarahan' => $perdarahan,
                    'tx' => $tx,
                    'bbl' => $bbl,
                    'bb' => $bb,
                    'pb' => $pb,
                    'lika' => $lika,
                    'imd' => $imd
                ]
            );


            $ibubersalin = IbuBersalinModel::all();
            return redirect('/laporan/ibubersalin')->with('ibubersalin', $ibubersalin);
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }



    public function tambahibubersalin()
    {
        $pasiens = PasienModel::all();
        return view('admin/tambahibubersalin', ["pasien" => $pasiens]);
    }

    public function update($id)
    {
        $ibubersalin = IbuBersalinModel::join('pasiens', 'pemeriksaanbersalin.id_pasien', '=', 'pasiens.id')
            ->where('pemeriksaanbersalin.id', $id)
            ->select(['pemeriksaanbersalin.*', 'pasiens.nama'])
            ->first();

        // $jadwal = JadwalModel::find($id);
        // print_r($jadwals);
        // exit;
        return view('admin/editibubersalin', ['ibubersalin' => $ibubersalin]);
    }

    public function editibubersalin(Request $request)
    {

        try {
            /* variabel form all */
            $id = $request->id;
            $tanggal = date("Y-m-d");
            $partograf = $request->partograf;
            $kala_1 = $request->kala_1;
            $proses_persalinan = $request->proses_persalinan;
            $tekanan_darah = $request->tekanan_darah;
            $nadi = $request->nadi;
            $suhu = $request->suhu;
            $cu = $request->cu;
            $perineum = $request->perineum;
            $perdarahan = $request->perdarahan;
            $tx = $request->tx;
            $bbl = $request->bbl;
            $bb = $request->bb;
            $pb = $request->pb;
            $lika = $request->lika;
            $imd = $request->imd;


            $ibubersalin = IbuBersalinModel::find($id);

            $ibubersalin->id = $id;
            $ibubersalin->tanggal = $tanggal;
            $ibubersalin->partograf = $partograf;
            $ibubersalin->kala_1 = $kala_1;
            $ibubersalin->proses_persalinan = $proses_persalinan;
            $ibubersalin->tekanan_darah = $tekanan_darah;
            $ibubersalin->nadi = $nadi;
            $ibubersalin->suhu = $suhu;
            $ibubersalin->cu = $cu;
            $ibubersalin->perineum = $perineum;
            $ibubersalin->perdarahan = $perdarahan;
            $ibubersalin->tx = $tx;
            $ibubersalin->bbl = $bbl;
            $ibubersalin->bb = $bb;
            $ibubersalin->pb = $pb;
            $ibubersalin->lika = $lika;
            $ibubersalin->imd = $imd;

            $ibubersalin->save();
            $ibubersalin = IbuBersalinModel::all();
            return redirect('/laporan/ibubersalin')->with('ibubersalin', $ibubersalin);
        } catch (Exception $error) {
            return "Gagal Diubah";
        }
    }


    public function delete($id)
    {
        $ibubersalin = IbuBersalinModel::find($id);
        $ibubersalin->link = "/laporan/ibubersalin/destroy/$id";
        return view('admin.modal.modalbersalin', ['ibubersalin' => $ibubersalin]);
    }
    public function destroy($id)
    {
        $ibubersalin = IbuBersalinModel::find($id);
        $ibubersalin->delete();
        $ibubersalin = IbuBersalinModel::all();
        return redirect('/laporan/ibubersalin')->with('ibubersalin', $ibubersalin);
    }
    public function export(Request $request)
    {
        $tanggal = isset($request->tahun) ? $request->tahun : date('Y');
        return Excel::download(new IbuBersalinExport($tanggal), 'ibubersalinexport.xlsx');
    }
}
