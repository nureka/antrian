<?php

namespace App\Http\Controllers;

use App\Exports\NifasExport;
use App\Models\NifasModel;
use App\Models\PasienModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class NifasController extends Controller
{
    //
    public function index(Request $request)
    {

        $nifas = NifasModel::join('pasiens', 'pemeriksaannifas.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaannifas.*, pasiens.nama, pasiens.alamat,  TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun'))
            ->where('nama', 'like', "%" . $request->keyword . "%")
            ->get();

        return view("admin/nifas", ["nifas" => $nifas]);
    }

    public function store(Request $request)
    {

        try {
            /* variabel form all */
            $id_pasien = $request->id_pasien;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $tekanan_darah = $request->tekanan_darah;
            $nadi = $request->nadi;
            $suhu = $request->suhu;
            $cu = $request->cu;
            $tfu = $request->tfu;
            $pendarahan = $request->pendarahan;
            $lochea = $request->lochea;
            $bab = $request->bab;
            $bak = $request->bak;
            $asi = $request->asi;
            $post_partum = $request->post_partum;
            $tx = $request->tx;
            $kie = $request->kie;
            // print_r($berat_badan);
            // exit;

            $nifas = NifasModel::create(
                [
                    'id_pasien' => $id_pasien,
                    'tanggal' => $tanggal,
                    'subject' => $subject,
                    'tekanan_darah' => $tekanan_darah,
                    'nadi' => $nadi,
                    'suhu' => $suhu,
                    'cu' => $cu,
                    'tfu' => $tfu,
                    'pendarahan' => $pendarahan,
                    'lochea' => $lochea,
                    'bab' => $bab,
                    'bak' => $bak,
                    'asi' => $asi,
                    'post_partum' => $post_partum,
                    'tx' => $tx,
                    'kie' => $kie
                ]
            );


            $nifas = NifasModel::all();
            return redirect('/laporan/nifas')->with('nifas', $nifas);
            // $antrian = AntrianModel::where("id_pasien", $id_pasien)->first();
            // $antrian->status = "Selesai";
            // $antrian->save();
            // $antrian = AntrianModel::all();
            // return redirect('/antrian')->with('antrian', $antrian);
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }



    public function tambahnifas()
    {
        $pasiens = PasienModel::all();
        return view('admin/tambahnifas', ["pasien" => $pasiens]);
    }

    public function update($id)
    {
        $nifas = NifasModel::join('pasiens', 'pemeriksaannifas.id_pasien', '=', 'pasiens.id')
            ->where('pemeriksaannifas.id', $id)
            ->select(['pemeriksaannifas.*', 'pasiens.nama'])
            ->first();

        // $jadwal = JadwalModel::find($id);
        // print_r($jadwals);
        // exit;
        return view('admin/editnifas', ['nifas' => $nifas]);
    }

    public function editnifas(Request $request)
    {

        try {
            /* variabel form all */
            $id = $request->id;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $tekanan_darah = $request->tekanan_darah;
            $nadi = $request->nadi;
            $suhu = $request->suhu;
            $cu = $request->cu;
            $tfu = $request->tfu;
            $pendarahan = $request->pendarahan;
            $lochea = $request->lochea;
            $bab = $request->bab;
            $bak = $request->bak;
            $asi = $request->asi;
            $post_partum = $request->post_partum;
            $tx = $request->tx;
            $kie = $request->kie;


            $nifas = NifasModel::find($id);

            $nifas->id = $id;
            $nifas->tanggal = $tanggal;
            $nifas->subject = $subject;
            $nifas->tekanan_darah = $tekanan_darah;
            $nifas->nadi = $nadi;
            $nifas->suhu = $suhu;
            $nifas->cu = $cu;
            $nifas->tfu = $tfu;
            $nifas->pendarahan = $pendarahan;
            $nifas->lochea = $lochea;
            $nifas->bab = $bab;
            $nifas->bak = $bak;
            $nifas->asi = $asi;
            $nifas->post_partum = $post_partum;
            $nifas->tx = $tx;
            $nifas->kie = $kie;

            $nifas->save();
            $nifas = NifasModel::all();
            return redirect('/laporan/nifas')->with('nifas', $nifas);
        } catch (Exception $error) {
            return "Gagal Diubah";
        }
    }


    public function delete($id)
    {
        $nifas = NifasModel::find($id);
        $nifas->link = "/laporan/nifas/destroy/$id";
        return view('admin.modal.modalnifas', ['nifas' => $nifas]);
    }
    public function destroy($id)
    {
        $nifas = NifasModel::find($id);
        $nifas->delete();
        $nifas = NifasModel::all();
        return redirect('/laporan/nifas')->with('nifas', $nifas);
    }
    public function export(Request $request)
    {
        $tanggal = isset($request->tahun) ? $request->tahun : date('Y');
        return Excel::download(new NifasExport($tanggal), 'nifasexport.xlsx');
    }
}
