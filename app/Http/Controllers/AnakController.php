<?php

namespace App\Http\Controllers;

use App\Exports\AnakExport;
use App\Models\AnakModel;
use App\Models\AntrianModel;
use App\Models\PasienModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AnakController extends Controller
{
    //menampilkan data 
    //nama alamat tgl lahir join pasien
    //time:mencari selisih waktu
    public function index(Request $request)
    {

        $anak = AnakModel::join('pasiens', 'pemeriksaananak.id_pasien', '=', 'pasiens.id')
            ->select(DB::raw('pemeriksaananak.*, pasiens.nama, pasiens.alamat,  TIMESTAMPDIFF( YEAR, pasiens.tanggal_lahir, now() )  as tahun,TIMESTAMPDIFF( MONTH, pasiens.tanggal_lahir, now() ) % 12 as bulan, 
            FLOOR( TIMESTAMPDIFF( DAY, pasiens.tanggal_lahir, now() ) % 30.4375 ) as hari'))
            ->where('nama', 'like', "%" . $request->keyword . "%")

            ->get();

        return view("admin/anak", ["anak" => $anak]);
    }

    //menyimpan laporan data
    public function store(Request $request)
    {

        try {
            /* variabel form all */
            $id_pasien = $request->id_pasien;
            $id_antrian = $request->id_antrian;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $berat_badan = $request->berat_badan;
            $tinggi_badan = $request->tinggi_badan;
            $tekanan_darah = $request->tekanan_darah;
            $suhu = $request->suhu;
            $rr = $request->rr;
            $nadi = $request->nadi;
            $status_gizi = $request->status_gizi;
            $sdidtk = $request->sdidtk;
            $mtbs = $request->mtbs;
            $imunisasi = $request->imunisasi;
            $tx = $request->tx;
            $kie = $request->kie;
            // print_r($berat_badan);
            // exit;

            //
            $anak = AnakModel::create(
                [
                    'id_pasien' => $id_pasien,
                    'tanggal' => $tanggal,
                    'subject' => $subject,
                    'berat_badan' => $berat_badan,
                    'tinggi_badan' => $tinggi_badan,
                    'tekanan_darah' => $tekanan_darah,
                    'suhu' => $suhu,
                    'rr' => $rr,
                    'nadi' => $nadi,
                    'status_gizi' => $status_gizi,
                    'sdidtk' => $sdidtk,
                    'mtbs' => $mtbs,
                    'imunisasi' => $imunisasi,
                    'tx' => $tx,
                    'kie' => $kie
                ]
            );

            //status berubah
            $antrian = AntrianModel::where('id', $id_antrian)->update(
                [
                    'status' => 'sudah diperiksa'
                ]
            );

            return redirect('/antrian');

            // $anak = AnakModel::all();
            // return redirect('/laporan/anak')->with('anak', $anak);
            // $antrian = AntrianModel::where("id_pasien", $id_pasien)->first();
            // $antrian->status = "Selesai";
            // $antrian->save();
            // $antrian = AntrianModel::all();
            // return redirect('/antrian')->with('antrian', $antrian);
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }


    //mengambil nama anak, tgl lahir
    //'anak' -> yg dipanggi di view
    public function tambahanak($id, $id_antrian)
    {
        $anak = PasienModel::where('pasiens.id', $id)
            ->select(['pasiens.*', 'pasiens.nama'])
            ->first();

        return view('admin/tambahanak', ["anak" => $anak, "id_antrian" => $id_antrian]);
    }

    //mengambil data pasien dan pemeriksaan anak dengan parameter id
    public function update($id)
    {
        $anak = AnakModel::join('pasiens', 'pemeriksaananak.id_pasien', '=', 'pasiens.id')
            ->where('pemeriksaananak.id', $id)
            ->select(['pemeriksaananak.*', 'pasiens.nama'])
            ->first();

        // $jadwal = JadwalModel::find($id);
        // print_r($jadwals);
        // exit;
        return view('admin/editanak', ['anak' => $anak]);
    }

    //menyimpan pembaruan data
    public function editanak(Request $request)
    {

        try {
            /* variabel form all */
            //variabel menginisialisasi request an user
            $id = $request->id;
            $tanggal = date("Y-m-d");
            $subject = $request->subject;
            $berat_badan = $request->berat_badan;
            $tinggi_badan = $request->tinggi_badan;
            $tekanan_darah = $request->tekanan_darah;
            $suhu = $request->suhu;
            $rr = $request->rr;
            $nadi = $request->nadi;
            $status_gizi = $request->status_gizi;
            $sdidtk = $request->sdidtk;
            $mtbs = $request->mtbs;
            $imunisasi = $request->imunisasi;
            $tx = $request->tx;
            $kie = $request->kie;

            //
            $anak = AnakModel::find($id);

            //set variabel untuk simpan ke db
            $anak->id = $id;
            $anak->tanggal = $tanggal;
            $anak->subject = $subject;
            $anak->berat_badan = $berat_badan;
            $anak->tinggi_badan = $tinggi_badan;
            $anak->tekanan_darah = $tekanan_darah;
            $anak->suhu = $suhu;
            $anak->rr = $rr;
            $anak->nadi = $nadi;
            $anak->status_gizi = $status_gizi;
            $anak->sdidtk = $sdidtk;
            $anak->mtbs = $mtbs;
            $anak->imunisasi = $imunisasi;
            $anak->tx = $tx;
            $anak->kie = $kie;

            $anak->save();
            $anak = AnakModel::all();
            return redirect('/laporan/anak')->with('anak', $anak);
        } catch (Exception $error) {
            return "Gagal Diubah";
        }
    }

    //mengambil id yg akan dihapus 
    //return menampilkan data pop up 
    public function delete($id)
    {
        $anak = AnakModel::find($id);
        $anak->link = "/laporan/anak/destroy/$id";
        return view('admin.modal.modalanak', ['anak' => $anak]);
    }

    //menghapus data dg parameter id
    public function destroy($id)
    {
        $anak = AnakModel::find($id);
        $anak->delete();
        $anak = AnakModel::all();
        return redirect('/laporan/anak')->with('anak', $anak);
    }

    //mengekspor
    public function export(Request $request)
    {
        $tanggal = isset($request->tahun) ? $request->tahun : date('Y');
        return Excel::download(new AnakExport($tanggal), 'anakexport.xlsx');
    }
}
