<?php

namespace App\Http\Controllers;

use App\Models\AntrianModel;
use App\Models\PasienModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use Illuminate\Support\Facades\Auth;
use JeroenNoten\LaravelAdminLte\Components\Form\Select;

class AntrianController extends Controller
{
    //menampilkan data antrian tgl asia jkt hari ini
    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');

        $antrians = AntrianModel::where('tanggal', '=', date('Y-m-d'))->get();
        return view("admin/antrian", ["antrian" => $antrians]);
    }

    //mengubah status sedang diperiksa jika diklik panggil
    public function updatestatus($id)
    {
        $antrian = AntrianModel::find($id);
        $antrian->status = "Sedang Diperiksa";
        $antrian->save();
        $antrians = AntrianModel::all();
        return redirect('/antrian')->with('antrian', $antrian);
    }

    //mengambil id untuk ditampilkan di pop up
    public function hapus($id)
    {
        $antrian = AntrianModel::find($id);


        $antrian->link = "/antrian/batal_/$id";
        return view('admin.modal.modalantrian', ['antrian' => $antrian]);
    }
    //mengubah status menjadi batal
    public function batal($id)
    {
        $antrian = AntrianModel::find($id);
        $antrian->status = "Batal Diperiksa";
        $antrian->save();
        //menampilkan data antrian hari ini
        date_default_timezone_set('Asia/Jakarta');

        $antrians = AntrianModel::where('tanggal', '=', date('Y-m-d'))->get();
        return redirect('/antrian')->with('antrian', $antrians);
    }

    public function create()
    {
        return view('jadwal_pemeriksaan');
    }
    public function create_anak($id)
    {
        return view('jadwal_pemeriksaan_anak', ['id' => $id]);
    }

    //menyimpan data antrian
    public function store(Request $request)
    {

        date_default_timezone_set('Asia/Jakarta');
        try {
            /* get no antrian */
            //mengambil data antrian berdasarkan tanggal request 
            //mengurutkan antrian dari yg terbesar
            $antrian = AntrianModel::where('tanggal', '=', date('Y-m-d', strtotime($request->tanggal)))->orderBy('no_antrian', 'desc')->first();
            //mengambil jam sekarang
            $jam = strtotime(date('H:i:s'));

            //jika antrian kosong maka mendapatkan no antrian nomor 1
            //jika tanggal request sama dengan hari ini maka dilakukan pengecekan lagi jika tidak mendapatkan jam antrian 06.00
            // jika jam dan menit kurang dari 06.00 maka akan mendapatkan jam antrian 06.00 jika tidak maka mendapatkan jam sekarang +20mnt
            //jika jam klik antrian < 11.30 maka mendapatkan antrian sesi pertama 06.00-11.30 jika tidak maka ikut sesi 2 (16.00)



            if ($antrian == null) {
                $no_antrian = 1;
                if (date('Y-m-d', strtotime($request->tanggal)) == date('Y-m-d')) {
                    if (date('H:i', strtotime($jam)) <= '06:00') {
                        $jam_tambah = '06:00:00';
                    } else {
                        $jam_tambah = date("H:i:s", strtotime('+20 minutes', $jam));
                    }
                    if (date('H:i', strtotime($jam_tambah)) < '11:30') {
                        $jam_tambah = $jam_tambah;
                    } else if (date('H:i', strtotime($jam_tambah)) > '16:00') {
                        $jam_tambah = $jam_tambah;
                    } else {
                        $jam_tambah = '16:00:00';
                    }
                } else {
                    $jam_tambah = '06:00:00';
                }

                //jika no antrian tidak kosong maka mendaptkan nomor antrian +1
                //jika antrian jam yg ada lebih dari jam saat ini atau tanggal antrian yg ada lebih dari tanggal hari ini maka
                // jam antrian terakhir ditambah 20mnt kemudian dilakukan pengecekan 
                // jika jam setelah pengecekan < 11.30 maka jam tetap
                //jika lebih dari 16.00 maka jam tetap

            } else {
                $no_antrian = $antrian->no_antrian + 1;
                if ($antrian->jam > date('H:i', $jam) || $antrian->tanggal > date('Y-m-d')) {
                    $jam_tambah = date("H:i:s", strtotime('+20 minutes', strtotime($antrian->jam)));
                } else {
                    $jam_tambah = date("H:i:s", strtotime('+20 minutes', $jam));
                }
                //116-122 ngecek sesi 
                if (date('H:i', strtotime($jam_tambah)) < '11:30') {
                    $jam_tambah = $jam_tambah;
                } else if (date('H:i', strtotime($jam_tambah)) > '16:00') {
                    $jam_tambah = $jam_tambah;
                } else {
                    $jam_tambah = '16:00:00';
                }
            }

            if ($jam_tambah >= '21:00:00') {
                return $this->callback2();
            }

            //jika pasien itu anak maka id pasien di tabel antrian adalah id pasien anak
            //jika tidak maka pasien ibu
            if (isset($request->id_anak)) {
                $id_pasien = $request->id_anak;
            } else {
                $id_pasien = PasienModel::where('id_users', '=', Auth::id())->select('id')->first();
                $id_pasien = $id_pasien->id;
            }


            $jenis_periksa = $request->jenis_periksa;
            $tanggal = date('Y-m-d', strtotime($request->tanggal));

            $no_antrian = $no_antrian;
            //menginisialisasi variabel yg akan disimpan
            $data = [
                'id_pasien' => $id_pasien,
                'tanggal' => $tanggal,
                'jenisperiksa' => $jenis_periksa,
                'jam' => $jam_tambah,
                'no_antrian' => $no_antrian
            ];
            // print_r($data);
            // exit;
            //menyimpan ke db
            $antrian = AntrianModel::create($data);
            $antrians = AntrianModel::find($antrian->id);
            // return redirect('/dashboard_daftar')->with('tes', $antrians);

            //return data antrian
            return $this->callback($antrians);
        } catch (Exception $error) {
            return "Gagal Disimpan";
        }
    }

    //menampilkan data setelah daftar antrian
    public function callback($data)
    {

        return view("dashboard_daftar", ["antrian" => $data]);
    }
    public function callback2()
    {

        return view("dashboard_pemberitahuan");
    }

    //menampilkan data untuk mencetak antrian
    public function callback_cetak($data)
    {
        // print_r($data);exit;
        //data dirubah ke json
        $data = json_decode($data, true);

        return view("dashboard_daftar_cetak", ["antrian" => $data]);
    }
}
