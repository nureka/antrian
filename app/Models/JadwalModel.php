<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JadwalModel extends Model
{
    use HasFactory;
    protected $table = 'jadwals';

    protected $fillable = [
        'id_pasien',
        'tanggal',
        'jenisperiksa',
        'jam',
        'jenisperiksa',
        'keterangan'
    ];



    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pasien()
    {
        return $this->belongsTo('App\Models\PasienModel');
    }
}
