<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IbuBersalinModel extends Model
{
    use HasFactory;
    protected $table = 'pemeriksaanbersalin';

    protected $fillable = [
        'id_pasien',
        'tanggal',
        'partograf',
        'kala_1',
        'proses_persalinan',
        'tekanan_darah',
        'nadi',
        'suhu',
        'cu',
        'perineum',
        'perdarahan',
        'tx',
        'bbl',
        'bb',
        'pb',
        'lika',
        'imd'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function pasien()
    {
        return $this->belongsTo('App\Models\PasienModel');
    }
}
