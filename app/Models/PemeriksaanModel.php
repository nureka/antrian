<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PemeriksaanModel extends Model
{
    use HasFactory;
    protected $table = 'pemeriksaans';

    protected $fillable = [
        'id_pasien',
        'tanggal',
        'keadaan_umum',
        'berat_badan',
        'tekanan_darah',
        'suhu',
        'nadi',
        'denyut_jantung',
        'tinggi_fundus_uteri'
    ];
}
