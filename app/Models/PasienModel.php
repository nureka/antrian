<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasienModel extends Model
{
    use HasFactory;
    protected $table = 'pasiens';

    protected $fillable = [
        'nama',
        'jk',
        'tanggal_lahir',
        'alamat',
        'id_users',
        'id_orangtua'

    ];



    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function jadwal()
    {
        return $this->hasMany('App\Models\JadwalModel');
    }

    public function ibuhamil()
    {
        return $this->hasMany('App\Models\IbuHamilModel');
    }

    public function ibubersalin()
    {
        return $this->hasMany('App\Models\IbuBersalinModel');
    }

    public function nifas()
    {
        return $this->hasMany('App\Models\NifasModel');
    }

    public function kb()
    {
        return $this->hasMany('App\Models\KBModel');
    }
}
