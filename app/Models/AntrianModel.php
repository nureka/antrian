<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AntrianModel extends Model
{
    use HasFactory;
    protected $table = 'antrians';
    protected $fillable = [
        'id_pasien',
        'tanggal',
        'jenisperiksa',
        'jam',
        'no_antrian',
    ];
}
