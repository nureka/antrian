<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BayiBLModel extends Model
{
    use HasFactory;
    protected $table = 'pemeriksaanbayibl';

    protected $fillable = [
        'id_pasien',
        'tanggal',
        'subject',
        'as',
        'berat_badan',
        'pb',
        'suhu',
        'lika',
        'lila',
        'anus',
        'ku',
        'status_gizi',
        'sdidtk',
        'mtbm',
        'mtbs',
        'imunisasi',
        'tx',
        'kie'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function pasien()
    {
        return $this->belongsTo('App\Models\PasienModel');
    }
}
