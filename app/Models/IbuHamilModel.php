<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IbuHamilModel extends Model
{
    use HasFactory;
    protected $table = 'pemeriksaanibuhamil';

    protected $fillable = [
        'id_pasien',
        'tanggal',
        'subject',
        'berat_badan',
        'lila',
        'tekanan_darah',
        'nadi',
        'suhu',
        'tinggi_fundus_uteri',
        'denyut_jantung',
        'let',
        'lab',
        'skor',
        'g',
        'tx',
        'kie'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function pasien()
    {
        return $this->belongsTo('App\Models\PasienModel');
    }
}
