<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NifasModel extends Model
{
    use HasFactory;
    protected $table = 'pemeriksaannifas';

    protected $fillable = [
        'id_pasien',
        'tanggal',
        'subject',
        'tekanan_darah',
        'nadi',
        'suhu',
        'cu',
        'tfu',
        'pendarahan',
        'lochea',
        'bab',
        'bak',
        'asi',
        'post_partum',
        'tx',
        'kie'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function pasien()
    {
        return $this->belongsTo('App\Models\PasienModel');
    }
}
