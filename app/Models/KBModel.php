<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KBModel extends Model
{
    use HasFactory;
    protected $table = 'pemeriksaankb';

    protected $fillable = [
        'id_pasien',
        'tanggal',
        'subject',
        'berat_badan',
        'tekanan_darah',
        'akseptor_kb',
        'tx',
        'kie'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function pasien()
    {
        return $this->belongsTo('App\Models\PasienModel');
    }
}
