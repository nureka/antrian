<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnakModel extends Model
{
    use HasFactory;
    protected $table = 'pemeriksaananak';

    protected $fillable = [
        'id_pasien',
        'tanggal',
        'subject',
        'berat_badan',
        'tinggi_badan',
        'tekanan_darah',
        'suhu',
        'rr',
        'nadi',
        'status_gizi',
        'sdidtk',
        'mtbs',
        'imunisasi',
        'tx',
        'kie'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function pasien()
    {
        return $this->belongsTo('App\Models\PasienModel');
    }
}
