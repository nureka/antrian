<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemeriksaanbayibl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pemeriksaanbayibl', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_pasien')->unsigned();
            $table->foreign('id_pasien')->references('id')->on('pasiens');
            $table->string('nama')->nullable();
            $table->date('tanggal');
            $table->string('subject')->nullable();
            $table->string('as')->nullable();
            $table->string('berat_badan')->nullable();
            $table->string('pb')->nullable();
            $table->string('suhu')->nullable();
            $table->string('lika')->nullable();
            $table->string('lila')->nullable();
            $table->string('anus')->nullable();
            $table->string('ku')->nullable();
            $table->string('status_gizi')->nullable();
            $table->string('sdidtk')->nullable();
            $table->string('mtbm')->nullable();
            $table->string('mtbs')->nullable();
            $table->string('imunisasi')->nullable();
            $table->string('tx')->nullable();
            $table->string('kie')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pemeriksaanbayibl');
    }
}
