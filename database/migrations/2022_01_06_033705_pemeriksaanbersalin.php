<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemeriksaanbersalin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pemeriksaanbersalin', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_pasien')->unsigned();
            $table->foreign('id_pasien')->references('id')->on('pasiens');
            $table->date('tanggal');
            $table->string('partograf')->nullable();
            $table->string('kala_1')->nullable();
            $table->string('proses_persalinan')->nullable();
            $table->string('tekanan_darah')->nullable();
            $table->string('nadi')->nullable();
            $table->string('suhu')->nullable();
            $table->string('cu')->nullable();
            $table->string('perineum')->nullable();
            $table->string('perdarahan')->nullable();
            $table->string('tx')->nullable();
            $table->string('bbl')->nullable();
            $table->string('bb')->nullable();
            $table->string('pb')->nullable();
            $table->string('lika')->nullable();
            $table->string('imd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pemeriksaanbersalin');
    }
}
