<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemeriksaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaanibuhamil', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_pasien')->unsigned();
            $table->foreign('id_pasien')->references('id')->on('pasiens');
            $table->date('tanggal');
            $table->string('subject')->nullable();
            $table->string('berat_badan')->nullable();
            $table->string('lila')->nullable();
            $table->string('tekanan_darah')->nullable();
            $table->string('nadi')->nullable();
            $table->string('suhu')->nullable();
            $table->string('tinggi_fundus_uteri')->nullable();
            $table->string('denyut_jantung')->nullable();
            $table->string('let')->nullable();
            $table->string('lab')->nullable();
            $table->string('skor')->nullable();
            $table->string('g')->nullable();
            $table->string('tx')->nullable();
            $table->string('kie')->nullable();
            $table->timestamps();
        });

        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaanibuhamil');
        //
    }
}
