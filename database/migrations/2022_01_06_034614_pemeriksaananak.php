<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemeriksaananak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pemeriksaananak', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_pasien')->unsigned();
            $table->foreign('id_pasien')->references('id')->on('pasiens');
            $table->date('tanggal');
            $table->string('subject')->nullable();
            $table->string('berat_badan')->nullable();
            $table->string('tinggi_badan')->nullable();
            $table->string('tekanan_darah')->nullable();
            $table->string('suhu')->nullable();
            $table->string('rr')->nullable();
            $table->string('nadi')->nullable();
            $table->string('status_gizi')->nullable();
            $table->string('sdidtk')->nullable();
            $table->string('mtbs')->nullable();
            $table->string('imunisasi')->nullable();
            $table->string('tx')->nullable();
            $table->string('kie')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pemeriksaananak');
    }
}
