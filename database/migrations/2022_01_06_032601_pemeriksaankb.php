<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemeriksaankb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pemeriksaankb', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_pasien')->unsigned();
            $table->foreign('id_pasien')->references('id')->on('pasiens');
            $table->date('tanggal');
            $table->string('subject')->nullable();
            $table->string('berat_badan')->nullable();
            $table->string('tekanan_darah')->nullable();
            $table->string('akseptor_kb')->nullable();
            $table->string('tx')->nullable();
            $table->string('kie')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pemeriksaankb');
    }
}
