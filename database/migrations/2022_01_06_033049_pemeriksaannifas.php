<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemeriksaannifas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pemeriksaannifas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_pasien')->unsigned();
            $table->foreign('id_pasien')->references('id')->on('pasiens');
            $table->date('tanggal');
            $table->string('subject')->nullable();
            $table->string('tekanan_darah')->nullable();
            $table->string('nadi')->nullable();
            $table->string('suhu')->nullable();
            $table->string('cu')->nullable();
            $table->string('tfu')->nullable();
            $table->string('pendarahan')->nullable();
            $table->string('lochea')->nullable();
            $table->string('bab')->nullable();
            $table->string('bak')->nullable();
            $table->string('asi')->nullable();
            $table->string('post_partum')->nullable();
            $table->string('tx')->nullable();
            $table->string('kie')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pemeriksaannifas');
    }
}
