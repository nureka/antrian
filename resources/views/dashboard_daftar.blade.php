@extends('head')
@section('tittle', 'Informasi Antrian')

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="container-fluid">
        <div style="height: 20px;"></div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <div class="card">
                    <div class="card-header ">
                        <h5 class="card-title m-0">Pendaftaran</h5>
                    </div>
                    <div class="card-body">
                        <h6 class="card-title">Anda Mendapat nomor : {{$antrian->no_antrian}}</h6>
                        <p class="card-text">Jenis Pemeriksaan : {{$antrian->jenisperiksa}}</p>
                        <p class="card-text">Estimasi Waktu Diperiksa : {{$antrian->tanggal}}, {{$antrian->jam}}</p>
                        <div class="row">
                            <div class="col-md-6 text-right">
                                <a href="/dashboard" class="btn btn-secondary">Kembali ke dashboard</a>
                            </div>
                            <div class="col-md-6 text-left">
                                <a class="btn btn-success" href="/cetak/{{$antrian}}" target="_blank">Cetak Antrian</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-4"></div>

        </div>
    </div>

</body>