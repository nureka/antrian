@extends('head')
@section('tittle', 'Register')

<body style="background-color: #153963;" class="hold-transition login-page">

  <div style="height: 10%;">

  </div>
  <div class="container">
    <div class="row">

      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="login-box">
          <!-- /.login-logo -->
          <div class="card card-outline">
            <div class="card-header text-center">
              <b>Form Regis Sistem Antrian PMB Yuliani</b>
            </div>
            <div class="card-body">

              <form action="{{ route('registers') }}" method="post">
                {{ csrf_field() }}
                <div class="input-group mb-3">
                  <input type="text" required id="nama" name="nama" class="form-control" placeholder="Nama">
                  <div class="input-group-append">

                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="email" required id="email" name="email" class="form-control" placeholder="Email">
                  <div class="input-group-append">

                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="password" required id="password" name="password" class="form-control" placeholder="Password">
                  <div class="input-group-append">

                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="text" onfocus="(this.type='date')" required id="tanggal_lahir" name="tanggal_lahir" class="form-control" placeholder="tanggal lahir">
                  <div class="input-group-append">

                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="text" required id="alamat" name="alamat" class="form-control" placeholder="Alamat">
                  <div class="input-group-append">

                  </div>
                </div>


                <div class="row">
                  @if(isset($data_))
                  <div class="col-12">
                    <div class="alert alert-danger">
                      <strong>{{ $data_ }}</strong>
                    </div>
                  </div>
                  @endif
                  <!-- /.col -->
                  <div class="col-4">
                    <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Register</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>


              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.login-box -->
        </div>

        <div class="col-md-3"></div>
      </div>

    </div>
    <!--     -->

    @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

    @section('js')
    <script>
      console.log('Hi!');
    </script>
    @stop