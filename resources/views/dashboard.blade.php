<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Dashboard</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="img/family.png" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/aos/aos.css" rel="stylesheet')}}">
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css">
    <link href="https://mdbootstrap.com/">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">
</head>



<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center ">
        <div class="container d-flex align-items-center justify-content-between">

            <div class="logo">
                <h1><a href="index.html">Sistem Antrian</a></h1>
            </div>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto" href="/dashboard">Dashboard</a></li>

                    <li><a class="nav-link scrollto" href="/logout">Logout</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->

    @extends('head')
    <div style="height: 70px;">

    </div>

    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="container-fluid">
            <div style="height: 20px;"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header ">
                            <h5 class="card-title m-0">Data Diri</h5>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">{{$data_pasien->nama}}</h6>

                            <p class="card-text">Tanggal Lahir : {{$data_pasien->tanggal_lahir}}</p>
                            <p class="card-text">Umur : {{$data_pasien->tahun}} Tahun {{$data_pasien->bulan}} Bulan {{$data_pasien->hari}} Hari </p>
                            <p class="card-text">Alamat : {{$data_pasien->alamat}}</p>

                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a href="/daftar_antrian" style="background-color: #153963; color:white" class="btn">Daftar Pemeriksaan</a>
                                    </div>
                                    <div class="col">
                                        <a class="btn btn-success" href="/jadwal_kontrol" target="_blank">Lihat Jadwal kontrol</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- jika ada data anak -->
                    @foreach($data_anak as $anak)
                    <div style="height: 20px;"></div>
                    <div class="card">
                        <div class="card-header ">
                            <h5 class="card-title m-0">Data Anak</h5>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">{{$anak->nama}}</h6>

                            <p class="card-text">umur : {{$anak->tahun}} Tahun {{$anak->bulan}} Bulan {{$anak->hari}} Hari </p>
                            <p class="card-text">jenis kelamin : {{$anak->jk}} </p>
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <!-- <a href="#" class="btn btn-primary">Daftar Pemeriksaan</a> -->
                                        <a href="/daftar_antrian_anak/{{$anak->id}}" style="background-color: #153963; color:white" class="btn">Daftar Pemeriksaan</a>
                                    </div>

                                    <!-- <div class="col">
                                    <a href="#" class="btn btn-success">Lihat Riyawat Pemeriksaan</a>
                                </div> -->
                                </div>
                            </div>

                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="col-md-6">

                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title m-0">Pengumuman</h5>
                        </div>
                        <div style="margin: 10px;">
                            <div class="alert alert-info" role="alert">
                                Jika ingin melakukan pemeriksaan anak,
                                harap menambah data anak terlebih dahulu dengan menekan tombol tambah dibawah ini
                            </div>
                            <div class="alert alert-info" role="alert">
                                pelayanan sesi 1 jam 06.00 sampai 11.00 <br>
                                pelayanan sesi 2 jam 16.00 sampai 21.00
                            </div>
                        </div>

                        <div style="height: 20px;"></div>
                        <div class="col-md-6">
                            <!-- <a href="#" class="btn btn-primary">Tambah Data Anak</a> -->
                            <button type="button" style="background-color: #153963; color:white" class="btn" data-toggle="modal" data-target="#staticBackdrop">
                                Tambah Data Anak
                            </button>
                        </div>
                        <div style="height: 20px;"></div>
                    </div>

                </div>
            </div>
        </div>

    </body>

    <div style="height: 40px;">

    </div>

    <!-- ======= Footer ======= -->
    <footer id="footer" style="background-color:#094570;">
        <div class="container">
            <h3>PMB YULIANI</h3>
            <p></p>
            <div class="social-links">
                <a href="https://twitter.com/hufsgsias" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="https://www.facebook.com/studyinkorea/" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>

        </div>
    </footer><!-- End Footer -->
    @extends('modal_tambah_anak')
    @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

    @section('js')
    <script>
        console.log('Hi!');
        var myVar = setInterval(myTimer, 1000);

        function myTimer() {
            var d = new Date();
            let ye = new Intl.DateTimeFormat('en', {
                year: 'numeric'
            }).format(d);
            let mo = new Intl.DateTimeFormat('en', {
                month: 'short'
            }).format(d);
            let da = new Intl.DateTimeFormat('en', {
                day: '2-digit'
            }).format(d);
            document.getElementById("jam_sekarang").innerHTML = "Sistem antrian PMB Yuliani " + `${da}-${mo}-${ye}` + ' ' + d.toLocaleTimeString() + "";
        };
    </script>
    @stop