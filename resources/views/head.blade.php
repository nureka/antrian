<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>@yield('tittle')</title>
<meta content="" name="description">
<meta content="" name="keywords">

<link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css')}}">
<script src="{{ asset('assets/frontend/js/jquery.slim.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/popper.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/bootstrap.min.js')}}"></script>
<link rel="stylesheet" href="{{ asset('assets/frontend/css/all.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/frontend/css/brands.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/frontend/css/fontawesome.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/frontend/css/svg-with-js.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/frontend/css/solid.min.css')}}" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="img/family.png" rel="icon">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600&display=swap" rel="stylesheet">
<? date_default_timezone_set('Asia/Jakarta'); ?>