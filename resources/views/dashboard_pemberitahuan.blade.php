@extends('head')
@section('tittle', 'Informasi Antrian')

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="container-fluid">
        <div style="height: 20px;"></div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <div class="card">
                    <div class="card-header ">
                        <h5 class="card-title m-0">Pendaftaran</h5>
                    </div>
                    <div class="card-body">
                        <h6 class="card-title">Pelayanan hari ini telah selesai, silahkan pilih tanggal berikutnya</h6>
                        <div class="row">
                            <div class="col-md-6 text-right">
                                <a href="/dashboard" class="btn btn-secondary">Kembali ke dashboard</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-4"></div>

        </div>
    </div>

</body>