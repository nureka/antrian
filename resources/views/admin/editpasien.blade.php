@extends('head')

<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <b>Edit Data Pasien</b>
            </div>
            <div class="card-body">

                <form action="/pasien/edit" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $pasien->id }}">
                    <tr>
                        <td><b>Nama</b></td>
                    </tr>
                    <div class="input-group mb-3">

                        <input required type="text" id="nama" name="nama" value="{{ $pasien->nama }}" class="form-control">
                        <div class="input-group-append">

                        </div>
                    </div>
                    <tr>
                        <td><b>Alamat</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="text" id="alamat" name="alamat" value="{{ $pasien->alamat }}" class="form-control">
                        <div class="input-group-append">

                        </div>
                    </div>

                    <!-- <tr>
                        <td><b>Umur</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="number" id="umur" name="umur" value="{{ $pasien->umur }}" class="form-control">
                        <div class="input-group-append">

                        </div>
                    </div> -->


                    <div class="row">

                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.login-box -->

        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop