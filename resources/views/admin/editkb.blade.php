@extends('head')

@section('tittle', 'Edit KB')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Edit Data laporan KB</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/kb/edit" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $kb->id }}">
                                <tr>
                                    <td><b>Nama</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $kb->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tanggal</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="date" disabled id="tanggal" name="tanggal" value="{{ $kb->tanggal }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Subject</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="subject" name="subject" value="{{ $kb->subject }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Berat Badan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="berat_badan" name="berat_badan" value="{{ $kb->berat_badan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tekanan Darah</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tekanan_darah" name="tekanan_darah" value="{{ $kb->tekanan_darah }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Akseptor KB</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="akseptor_kb" name="akseptor_kb" value="{{ $kb->akseptor_kb }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>TX</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" value="{{ $kb->tx }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>KIE</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="kie" name="kie" value="{{ $kb->kie }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop