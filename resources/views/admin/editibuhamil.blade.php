@extends('head')

@section('tittle', 'Edit Ibu Hamil')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Edit Data laporan Ibu Hamil</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/ibuhamil/edit" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $ibuhamil->id }}">
                                <tr>
                                    <td><b>Nama</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $ibuhamil->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tanggal</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="date" disabled id="tanggal" name="tanggal" value="{{ $ibuhamil->tanggal }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Subject</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="subject" name="subject" value="{{ $ibuhamil->subject }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Berat Badan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="berat_badan" name="berat_badan" value="{{ $ibuhamil->berat_badan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>LILA</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="lila" name="lila" value="{{ $ibuhamil->lila }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Tekanan Darah</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tekanan_darah" name="tekanan_darah" value="{{ $ibuhamil->tekanan_darah }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Nadi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="nadi" name="nadi" value="{{ $ibuhamil->nadi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Suhu</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" value="{{ $ibuhamil->suhu }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>TFU</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tinggi_fundus_uteri" name="tinggi_fundus_uteri" value="{{ $ibuhamil->tinggi_fundus_uteri }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>DJU</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="denyut_jantung" name="denyut_jantung" value="{{ $ibuhamil->denyut_jantung }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>LET</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="let" name="let" value="{{ $ibuhamil->let }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>LAB</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="lab" name="lab" value="{{ $ibuhamil->lab }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Skor</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="skor" name="skor" value="{{ $ibuhamil->skor }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>G</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="g" name="g" value="{{ $ibuhamil->g }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>TX</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" value="{{ $ibuhamil->tx }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>KIE</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="kie" name="kie" value="{{ $ibuhamil->kie }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop