@extends('head')

@section('tittle', 'Tambah Ibu Bersalin')

<body style="background-color: #153963;" class="hold-transition login-page">

    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">

                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Tambah Data Laporan Ibu Bersalin</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/ibubersalin/tambah" method="post">
                                {{ csrf_field() }}
                                <div class="input-group mb-3">
                                    <select class="custom-select custom-select-md mb-3" id="id_pasien" name="id_pasien" aria-label="Default select example">
                                        @foreach($pasien as $p)
                                        <option value="{{ $p->id }}">{{ $p->nama}}</option>

                                        @endforeach
                                    </select>
                                    <!-- <input required type="text" id="nama" name="nama" class="form-control" placeholder="Nama"> -->

                                </div>

                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="date" name="tanggal" class="form-control" placeholder="tanggal">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="partograf" name="partograf" class="form-control" placeholder="Partograf">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="kala_1" name="kala_1" class="form-control" placeholder="Kala 1">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="proses_persalinan" name="proses_persalinan" class="form-control" placeholder="Proses Persalinan">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="tekanan_darah" name="tekanan_darah" class="form-control" placeholder="TD">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="nadi" name="nadi" class="form-control" placeholder="Nadi">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" class="form-control" placeholder="Suhu">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="cu" name="cu" class="form-control" placeholder="CU">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="perineum" name="perineum" class="form-control" placeholder="Perineum">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="perdarahan" name="perdarahan" class="form-control" placeholder="Perdarahan">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" class="form-control" placeholder="TX">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="bbl" name="bbl" class="form-control" placeholder="BBL">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="bb" name="bb" class="form-control" placeholder="BB">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="pb" name="pb" class="form-control" placeholder="PB">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="lika" name="lika" class="form-control" placeholder="LIKA">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="imd" name="imd" class="form-control" placeholder="IMD">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>


                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>

        <!--     -->

        <script>
            n = new Date();
            y = n.getFullYear();
            m = n.getMonth() + 1;
            d = n.getDate();
            document.getElementById("date").value = y + "/" + m + "/" + d;
        </script>
        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop