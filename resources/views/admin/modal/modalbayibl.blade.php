<form action="{{$bayibl->link}}" method="get">
    <div class="modal-body">
        @csrf
        @method('DELETE')
        <h5 class="text-center">Apakah Anda Yakin Menghapus Data Ini?</h5>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-danger">Yes, Delete</button>
    </div>
</form>