@if(!$ibuhamil->isEmpty())

<h5><strong>Ibu Hamil</strong>

</h5>
<table class="table table-striped ">


    <tr>

        <td>Tanggal</td>
        <td>Detail</td>
    </tr>
    @foreach($ibuhamil as $a)
    <tr>
        <td>{{ $a->tanggal}}</td>
        <td>
            subject : {{$a->subject}} <br>
            berat_badan : {{$a->berat_badan}} <br>
            lila : {{$a->lila}} <br>
            tekanan_darah : {{$a->tekanan_darah}} <br>
            nadi : {{$a->nadi}} <br>
            suhu : {{$a->suhu}} <br>
            tinggi_fundus_uteri : {{$a->tinggi_fundus_uteri}} <br>
            denyut_jantung : {{$a->denyut_jantung}} <br>
            let : {{$a->let}} <br>
            lab : {{$a->lab}} <br>
            skor : {{$a->skor}} <br>
            g : {{$a->g}} <br>
            tx : {{$a->tx}} <br>
            kie : {{$a->kie}} <br>

        </td>
        <td></td>
    </tr>
    @endforeach
</table>
@endif

@if(!$ibubersalin->isEmpty())
<h5><strong>Ibu Bersalin</strong>

</h5>
<table class="table table-striped ">


    <tr>

        <td>Tanggal</td>
        <td>Detail</td>
    </tr>
    @foreach($ibubersalin as $a)
    <tr>

        <td>{{ $a->tanggal}}</td>
        <td>
            partograf : {{$a->partograf}} <br>
            kala_1 : {{$a->kala_1}} <br>
            proses_persalinan : {{$a->proses_persalinan}} <br>
            tekanan_darah : {{$a->tekanan_darah}} <br>
            nadi : {{$a->nadi}} <br>
            suhu : {{$a->suhu}} <br>
            cu : {{$a->cu}} <br>
            perineum : {{$a->perineum}} <br>
            perdarahan : {{$a->perdarahan}} <br>
            tx : {{$a->tx}} <br>
            bbl : {{$a->bbl}} <br>
            bb : {{$a->bb}} <br>
            pb : {{$a->pb}} <br>
            lika : {{$a->lika}} <br>
            imd : {{$a->imd}} <br>

        </td>
        <td></td>
    </tr>
    @endforeach
</table>
@endif
@if(!$nifas->isEmpty())
<h5><strong>Nifas</strong>

</h5>
<table class="table table-striped ">


    <tr>

        <td>Tanggal</td>
        <td>Detail</td>
    </tr>
    @foreach($nifas as $a)
    <tr>

        <td>{{ $a->tanggal}}</td>
        <td>
            subject : {{$a->subject}} <br>
            tekanan_darah : {{$a->tekanan_darah}} <br>
            nadi : {{$a->nadi}} <br>
            suhu : {{$a->suhu}} <br>
            cu : {{$a->cu}} <br>
            tfu : {{$a->tfu}} <br>
            pendarahan : {{$a->pendarahan}} <br>
            lochea : {{$a->lochea}} <br>
            bab : {{$a->bab}} <br>
            bak : {{$a->bak}} <br>
            asi : {{$a->asi}} <br>
            post_partum : {{$a->post_partum}} <br>
            tx : {{$a->tx}} <br>
            kie : {{$a->kie}} <br>

        </td>
        <td></td>
    </tr>
    @endforeach
</table>
@endif
@if(!$kb->isEmpty())
<h5><strong>Keluarga Berencana</strong>

</h5>
<table class="table table-striped ">


    <tr>

        <td>Tanggal</td>
        <td>Detail</td>
    </tr>
    @foreach($kb as $a)
    <tr>

        <td>{{ $a->tanggal}}</td>
        <td>
            subject : {{$a->subject}} <br>
            berat_badan : {{$a->berat_badan}} <br>
            tekanan_darah : {{$a->tekanan_darah}} <br>
            akseptor_kb : {{$a->akseptor_kb}} <br>
            tx : {{$a->tx}} <br>
            kie : {{$a->kie}} <br>

        </td>
        <td></td>
    </tr>
    @endforeach
</table>
@endif
@if(!$pemeriksaan_anak->isEmpty())
<h5><strong>Pemeriksaan Anak</strong>

</h5>
<table class="table table-striped ">


    <tr>

        <td>Tanggal</td>
        <td>Detail</td>
    </tr>
    @foreach($pemeriksaan_anak as $a)
    <tr>

        <td>{{ $a->tanggal}}</td>
        <td>
            subject : {{$a->subject}} <br>
            berat_badan : {{$a->berat_badan}} <br>
            tinggi_badan : {{$a->tinggi_badan}} <br>
            tekanan_darah : {{$a->tekanan_darah}} <br>
            suhu : {{$a->suhu}} <br>
            rr : {{$a->rr}} <br>
            nadi : {{$a->nadi}} <br>
            status_gizi : {{$a->status_gizi}} <br>
            sdidtk : {{$a->sdidtk}} <br>
            mtbs : {{$a->mtbs}} <br>
            imunisasi : {{$a->imunisasi}} <br>
            tx : {{$a->tx}} <br>
            kie : {{$a->kie}} <br>

        </td>
        <td></td>
    </tr>
    @endforeach
</table>
@endif
@if(!$bayibarulahir->isEmpty())
<h5><strong>Bayi Baru Lahir</strong>

</h5>
<table class="table table-striped ">


    <tr>

        <td>Tanggal</td>
        <td>Detail</td>
    </tr>
    @foreach($bayibarulahir as $a)
    <tr>

        <td>{{ $a->tanggal}}</td>
        <td>
            subject : {{$a->subject}} <br>
            as : {{$a->as}} <br>
            lila : {{$a->lila}} <br>
            berat_badan : {{$a->berat_badan}} <br>
            pb : {{$a->pb}} <br>
            suhu : {{$a->suhu}} <br>
            lika : {{$a->lika}} <br>
            lila : {{$a->lila}} <br>
            anus : {{$a->anus}} <br>
            status_gizi : {{$a->status_gizi}} <br>
            sdidtk : {{$a->sdidtk}} <br>
            mtbm : {{$a->mtbm}} <br>
            mtbs : {{$a->mtbs}} <br>
            imunisasi : {{$a->imunisasi}} <br>
            tx : {{$a->tx}} <br>
            kie : {{$a->kie}} <br>

        </td>
        <td></td>
    </tr>
    @endforeach
</table>
@endif
<style>
    .scheduler-border {
        width: inherit;
        /* Or auto */
        padding: 0 10px;
        /* To give a bit of padding on the left and right */
        border-bottom: none;
    }
</style>