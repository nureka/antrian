@extends('head')

<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <b>Tambah Data Hasil Pemeriksaan</b>
            </div>
            <div class="card-body">

                <form action="/pemeriksaan/tambah" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_pasien" value="{{ $id_pasien }}">
                    <div class="input-group mb-3">

                        <!-- <input required type="text" id="nama" name="nama" class="form-control" placeholder="Nama"> -->

                    </div>

                    <div class="input-group mb-3">
                        <input required type="text" disabled id="date" name="tanggal" class="form-control" placeholder="tanggal">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input required type="text" id="keadaan_umum" name="keadaan_umum" class="form-control" placeholder="keadaan umum">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input required type="number" id="berat_badan" name="berat_badan" class="form-control" placeholder="berat badan">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input required type="number" id="tekanan_darah" name="tekanan_darah" class="form-control" placeholder="tekanan darah">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input required type="number" id="suhu" name="suhu" class="form-control" placeholder="suhu">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input required type="number" id="nadi" name="nadi" class="form-control" placeholder="nadi">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input required type="number" id="denyut_jantung" name="denyut_jantung" class="form-control" placeholder="denyut jantung">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input required type="number" id="tinggi_fundus_uteri" name="tinggi_fundus_uteri" class="form-control" placeholder="tinggi fundus uteri">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>


                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.login-box -->

        <!--     -->

        <script>
            n = new Date();
            y = n.getFullYear();
            m = n.getMonth() + 1;
            d = n.getDate();
            document.getElementById("date").value = y + "/" + m + "/" + d;
        </script>
        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop