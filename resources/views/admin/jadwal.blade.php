@extends('admin/mainadmin')

@section('tittle', 'Jadwal')

@section('cont')
<section id="jadwal" class="services">
    <div class="container">

        <div class="section-title" data-aos="zoom-out" style="margin-top:6%;">
            <h2>Data</h2>
            <p>Jadwal Kontrol Pasien</p>
        </div>
        <form action="{{ url()->current() }}" method="get">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">

                        <input type="search" class="form-control" autocomplete="false" name="keyword" value="{{ request('keyword') }}" placeholder="Cari Nama Pasien">

                    </div>
                    <div class="col-md-4">
                        <button type="submit" class="btn" style="background-color: #153963; color:white">Cari Nama Pasien
                        </button>

                    </div>
                    <div class="col-md-5">

                    </div>

                </div>

            </div>
        </form>
        <div class="container">
            <a href="/jadwaladm/tambah"> + Tambah Jadwal Baru</a>
            <div class="col-lg-20">
                <div style="overflow: auto;">
                    <table class="table table-striped ">
                        <thead>
                            <tr>

                                <th scope="col">Nama Pasien</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Jam</th>
                                <th scope="col">Jenis Periksa</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($jadwal as $j)
                            <tr>
                                <td>{{$j->nama}}</td>
                                <td>{{$j->tanggal}}</td>
                                <td>{{$j->jam}}</td>
                                <td>{{$j->jenisperiksa}}</td>
                                <td>{{$j->keterangan}}</td>
                                <td>
                                    <a href="/jadwal/edit/{{ $j->id }}">Edit</a>
                                    |
                                    <a class="btn btn-danger" data-toggle="modal" id="smallButton" data-target="#smallModal" data-attr="/jadwal/hapus/{{ $j->id }}" title="Delete Project">
                                        Hapus
                                    </a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
</section>
@endsection

<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="smallBody">
                <div>
                    <!-- the result to be displayed apply here -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    // display a modal (small modal)
    $(document).on('click', '#smallButton', function(event) {
        event.preventDefault();
        let href = $(this).attr('data-attr');
        $.ajax({
            url: href,
            beforeSend: function() {
                $('#loader').show();
            },
            // return the result
            success: function(result) {
                $('#smallModal').modal("show");
                $('#smallBody').html(result).show();
            },
            complete: function() {
                $('#loader').hide();
            },
            error: function(jqXHR, testStatus, error) {
                console.log(error);
                alert("Page " + href + " cannot open. Error:" + error);
                $('#loader').hide();
            },
            timeout: 8000
        })
    });
</script>