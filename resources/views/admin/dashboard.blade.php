@extends('admin/mainadmin')

@section('tittle', 'Dashboard')

@section('cont')
<section id="dashboard" class="services">
    <div class="container">
        <div style="height: 20px;"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Statistik Pasien</h1>
                    <!-- <p>Full blog post details <a href="http://flopreynat.com/blog//make-google-charts-responsive.html">on my blog</a></p> -->
                </div>
                <div class="col-md-4 col-md-offset-4">
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
                <div class="col-md-6">
                    <div id="chartContainer2" style="height: 300px; width: 100%;"></div>
                </div>

            </div>
        </div>
</section>
@endsection
<!-- <script src="https://www.google.com/jsapi"></script> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="{{ asset('assets/js/canvasjs.min.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
<style>

</style>
<script>
    getapi();
    getapi_kelahiran();

    function show(data) {
        response_data = data.data;
        console.log(response_data[0].anak);


        var chart = new CanvasJS.Chart("chartContainer", {
            theme: "light2",
            animationEnabled: true,
            title: {
                // text: "Game of Thrones Viewers of the First Airing on HBO"
            },
            axisY: {
                title: "Jumlah Pasien",
            },
            toolTip: {
                shared: "true"
            },
            legend: {
                cursor: "pointer",
                itemclick: toggleDataSeries
            },
            data: [{
                    type: "spline",
                    visible: true,
                    showInLegend: true,
                    indexLabelFontSize: 16,
                    name: "Pemeriksaan Ibu",
                    dataPoints: [{
                            label: response_data[0].bulan,
                            y: response_data[0].ibu
                        },
                        {
                            label: response_data[1].bulan,
                            y: response_data[1].ibu
                        },
                        {
                            label: response_data[2].bulan,
                            y: response_data[2].ibu
                        },
                        {
                            label: response_data[3].bulan,
                            y: response_data[3].ibu
                        },
                        {
                            label: response_data[4].bulan,
                            y: response_data[4].ibu
                        },
                        {
                            label: response_data[5].bulan,
                            y: response_data[5].ibu
                        },
                        // {
                        //     label: response_data[6].bulan,
                        //     y: response_data[6].ibu
                        // },
                        // {
                        //     label: response_data[7].bulan,
                        //     y: response_data[7].ibu
                        // },
                        // {
                        //     label: response_data[8].bulan,
                        //     y: response_data[8].ibu
                        // },
                        // {
                        //     label: response_data[9].bulan,
                        //     y: response_data[9].ibu
                        // },
                        // {
                        //     label: response_data[10].bulan,
                        //     y: response_data[10].ibu
                        // },
                        // {
                        //     label: response_data[11].bulan,
                        //     y: response_data[11].ibu
                        // }
                    ]
                },
                {
                    type: "spline",
                    visible: true,
                    showInLegend: true,
                    indexLabelFontSize: 16,
                    name: "Pemeriksaan Anak",
                    dataPoints: [{
                            label: response_data[0].bulan,
                            y: response_data[0].anak

                        },
                        {
                            label: response_data[1].bulan,
                            y: response_data[1].anak
                        },
                        {
                            label: response_data[2].bulan,
                            y: response_data[2].anak
                        },
                        {
                            label: response_data[3].bulan,
                            y: response_data[3].anak
                        },
                        {
                            label: response_data[4].bulan,
                            y: response_data[4].anak
                        },
                        {
                            label: response_data[5].bulan,
                            y: response_data[5].anak
                        }
                        // {
                        //     y: response_data[6].anak
                        // },
                        // {
                        //     y: response_data[7].anak
                        // },
                        // {
                        //     y: response_data[8].anak
                        // },
                        // {
                        //     y: response_data[9].anak
                        // },
                        // {
                        //     y: response_data[10].anak
                        // },
                        // {
                        //     y: response_data[11].anak
                        // }
                    ]
                }
            ]
        });
        chart.render();

        function toggleDataSeries(e) {
            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
            } else {
                e.dataSeries.visible = true;
            }
            chart.render();
        }
    }


    function show_kelahiran(data) {
        response_data = data.data;
        var chart = new CanvasJS.Chart("chartContainer2", {
            animationEnabled: true,
            title: {
                // text: "Number of iPhones Sold in Different Quarters"
            },
            axisX: {

            },
            axisY: {
                title: " Kelahiran",
                titleFontColor: "#4F81BC",
                includeZero: true,
                suffix: ""
            },
            data: [{
                indexLabelFontColor: "darkSlateGray",
                name: "views",
                type: "area",
                yValueFormatString: "# bayi",
                dataPoints: [{
                        // x: response_data[0].tahun,
                        y: response_data[0].kelahiran,
                        label: response_data[0].tahun
                    },
                    {
                        // x: response_data[1].tahun,
                        y: response_data[1].kelahiran,
                        label: response_data[1].tahun
                    },
                    {
                        // x: response_data[2].tahun,
                        y: response_data[2].kelahiran,
                        label: response_data[2].tahun
                    },
                    {
                        // x: response_data[3].tahun,
                        y: response_data[3].kelahiran,
                        label: response_data[3].tahun
                    },
                    {
                        // x: response_data[4].tahun,
                        y: response_data[4].kelahiran,
                        label: response_data[4].tahun
                    },
                    {
                        // x: response_data[5].tahun,
                        y: response_data[5].kelahiran,
                        label: response_data[5].tahun
                    }
                ]
            }]
        });
        chart.render();
    }



    // $(window).resize(function() {
    //     drawChart1();
    //     drawChart2();
    // });

    async function getapi() {
        var url = 'data_dashboard';
        // Storing response
        const response = await fetch('http://127.0.0.1:8000/' + url);

        // Storing data in form of JSON
        var data = await response.json();
        show(data);
    }

    async function getapi_kelahiran() {
        var url = 'data_dashboard_kelahiran';
        // Storing response
        const response = await fetch('http://127.0.0.1:8000/' + url);

        // Storing data in form of JSON
        var data = await response.json();

        show_kelahiran(data);
    }
    // Reminder: you need to put https://www.google.com/jsapi in the head of your document or as an external resource on codepen //
</script>