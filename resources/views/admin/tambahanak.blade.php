@extends('head')

@section('tittle', 'Tambah Anak')

<body style="background-color: #153963;" class="hold-transition login-page">


    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Tambah Data Laporan Anak</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/anak/tambah" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_pasien" value="{{ $anak->id }}">
                                <input type="hidden" name="id_antrian" value="{{ $id_antrian }}">
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $anak->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="date" name="tanggal" class="form-control" placeholder="tanggal">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="subject" name="subject" class="form-control" placeholder="subject">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="berat_badan" name="berat_badan" class="form-control" placeholder="BB">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="tinggi_badan" name="tinggi_badan" class="form-control" placeholder="TB">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="tekanan_darah" name="tekanan_darah" class="form-control" placeholder="TD">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" class="form-control" placeholder="Suhu">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="rr" name="rr" class="form-control" placeholder="RR">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="nadi" name="nadi" class="form-control" placeholder="Nadi">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="status_gizi" name="status_gizi" class="form-control" placeholder="Status Gizi">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="sdidtk" name="sdidtk" class="form-control" placeholder="SDIDTK">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="mtbs" name="mtbs" class="form-control" placeholder="MTBS">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="imunisasi" name="imunisasi" class="form-control" placeholder="Imunisasi">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" class="form-control" placeholder="TX">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="kie" name="kie" class="form-control" placeholder="KIE">
                                    <div class="input-group-append">

                                    </div>
                                </div>



                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>


                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        <script>
            n = new Date();
            y = n.getFullYear();
            m = n.getMonth() + 1;
            d = n.getDate();
            document.getElementById("date").value = y + "/" + m + "/" + d;
        </script>
        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop