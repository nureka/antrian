@extends('head')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline card-primary">
                        <div class="card-header text-center">
                            <b>Tambah Data Pasien</b>
                        </div>
                        <div class="card-body">

                            <form action="/pasien/tambah" method="post">
                                {{ csrf_field() }}
                                <div class="input-group mb-3">
                                    <input required type="text" id="nama" name="nama" class="form-control" placeholder="Nama">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-envelope"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="input-group mb-3">
                                    <input required type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-lock"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input required type="number" id="umur" name="umur" class="form-control" placeholder="umur">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-lock"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input required type="text" id="pekerjaan" name="pekerjaan" class="form-control" placeholder="pekerjaan">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-lock"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>


                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop