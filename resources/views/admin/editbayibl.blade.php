@extends('head')

@section('tittle', 'Edit Bayi Baru Lahir')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Edit Data laporan Bayi Baru Lahir</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/bayibl/edit" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $bayibl->id }}">
                                <tr>
                                    <td><b>Nama</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $bayibl->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tanggal</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="date" disabled id="tanggal" name="tanggal" value="{{ $bayibl->tanggal }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Subject</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="subject" name="subject" value="{{ $bayibl->subject }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>AS</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="as" name="as" value="{{ $bayibl->as }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Berat Badan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="berat_badan" name="berat_badan" value="{{ $bayibl->berat_badan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>PB</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="pb" name="pb" value="{{ $bayibl->pb }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Suhu</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" value="{{ $bayibl->suhu }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>LIKA</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="lika" name="lika" value="{{ $bayibl->lika }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>LILA</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="lila" name="lila" value="{{ $bayibl->lila }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Anus</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="anus" name="anus" value="{{ $bayibl->anus }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>KU</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="ku" name="ku" value="{{ $bayibl->ku }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Status Gizi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="status_gizi" name="status_gizi" value="{{ $bayibl->status_gizi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>SDIDTK</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="sdidtk" name="sdidtk" value="{{ $bayibl->sdidtk }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>MTBM</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="mtbm" name="mtbm" value="{{ $bayibl->mtbm }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>MTBS</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="mtbs" name="mtbs" value="{{ $bayibl->mtbs }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Imunisasi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="imunisasi" name="imunisasi" value="{{ $bayibl->imunisasi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <tr>
                                    <td><b>TX</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" value="{{ $bayibl->tx }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>KIE</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="kie" name="kie" value="{{ $bayibl->kie }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop