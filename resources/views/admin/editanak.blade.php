@extends('head')

@section('tittle', 'Edit Anak')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Edit Data laporan Anak</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/anak/edit" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $anak->id }}">
                                <tr>
                                    <td><b>Nama</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $anak->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tanggal</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="date" disabled id="tanggal" name="tanggal" value="{{ $anak->tanggal }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Subject</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="subject" name="subject" value="{{ $anak->subject }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Berat Badan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="berat_badan" name="berat_badan" value="{{ $anak->berat_badan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tinggi Badan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tinggi_badan" name="tinggi_badan" value="{{ $anak->tinggi_badan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Tekanan Darah</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tekanan_darah" name="tekanan_darah" value="{{ $anak->tekanan_darah }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Suhu</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" value="{{ $anak->suhu }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>RR</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="rr" name="rr" value="{{ $anak->rr }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Nadi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="nadi" name="nadi" value="{{ $anak->nadi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Status Gizi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="status_gizi" name="status_gizi" value="{{ $anak->status_gizi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>SDIDTK</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="sdidtk" name="sdidtk" value="{{ $anak->sdidtk }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>MTBS</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="mtbs" name="mtbs" value="{{ $anak->mtbs }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Imunisasi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="imunisasi" name="imunisasi" value="{{ $anak->imunisasi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <tr>
                                    <td><b>TX</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" value="{{ $anak->tx }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>KIE</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="kie" name="kie" value="{{ $anak->kie }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop