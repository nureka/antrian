@extends('head')

@section('tittle', 'Tambah Bayu Baru Lahir')

<body style="background-color: #153963;" class="hold-transition login-page">

    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">

                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Tambah Data Laporan Bayi Baru Lahir</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/bayibl/tambah" method="post">
                                {{ csrf_field() }}
                                <div class="input-group mb-3">
                                    <select class="custom-select custom-select-md mb-3" id="id_pasien" name="id_pasien" aria-label="Default select example">
                                        @foreach($pasien as $p)
                                        <option value="{{ $p->id }}">{{ $p->nama}}</option>

                                        @endforeach
                                    </select>
                                    <!-- <input required type="text" id="nama" name="nama" class="form-control" placeholder="Nama"> -->

                                </div>

                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="date" name="tanggal" class="form-control" placeholder="tanggal">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="subject" name="subject" class="form-control" placeholder="subject">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="as" name="as" class="form-control" placeholder="AS">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="berat_badan" name="berat_badan" class="form-control" placeholder="BB">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="pb" name="pb" class="form-control" placeholder="PB">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" class="form-control" placeholder="Suhu">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="lika" name="lika" class="form-control" placeholder="LIKA">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="lila" name="lila" class="form-control" placeholder="LILA">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="anus" name="anus" class="form-control" placeholder="Nadi">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="ku" name="ku" class="form-control" placeholder="KU">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="status_gizi" name="status_gizi" class="form-control" placeholder="Status Gizi">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="sdidtk" name="sdidtk" class="form-control" placeholder="SDIDTK">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="mtbm" name="mtbm" class="form-control" placeholder="MTBM">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="mtbs" name="mtbs" class="form-control" placeholder="MTBS">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="imunisasi" name="imunisasi" class="form-control" placeholder="Imunisasi">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" class="form-control" placeholder="TX">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="kie" name="kie" class="form-control" placeholder="KIE">
                                    <div class="input-group-append">

                                    </div>
                                </div>



                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>


                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->

                </div>

                <div class="col-md-3"></div>


            </div>

            <!--     -->
            <script>
                n = new Date();
                y = n.getFullYear();
                m = n.getMonth() + 1;
                d = n.getDate();
                document.getElementById("date").value = y + "/" + m + "/" + d;
            </script>
            @section('css')
            <link rel="stylesheet" href="/css/admin_custom.css">
            @stop

            @section('js')
            <script>
                console.log('Hi!');
            </script>
            @stop