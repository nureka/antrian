@extends('head')

@section('tittle', 'Tambah Ibu Hamil')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">

                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Tambah Data Laporan Ibu Hamil</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/ibuhamil/tambah" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_pasien" value="{{ $ibuhamil->id }}">
                                <input type="hidden" name="id_antrian" value="{{ $id_antrian }}">
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $ibuhamil->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="date" name="tanggal" class="form-control" placeholder="tanggal">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="subject" name="subject" class="form-control" placeholder="subject">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="berat_badan" name="berat_badan" class="form-control" placeholder="BB">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="lila" name="lila" class="form-control" placeholder="LILA">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="tekanan_darah" name="tekanan_darah" class="form-control" placeholder="TD">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="nadi" name="nadi" class="form-control" placeholder="Nadi">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" class="form-control" placeholder="Suhu">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="tinggi_fundus_uteri" name="tinggi_fundus_uteri" class="form-control" placeholder="TFU">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="denyut_jantung" name="denyut_jantung" class="form-control" placeholder="DJU">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="let" name="let" class="form-control" placeholder="LET">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="lab" name="lab" class="form-control" placeholder="LAB">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="skor" name="skor" class="form-control" placeholder="Skor">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="g" name="g" class="form-control" placeholder="G">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" class="form-control" placeholder="TX">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" id="kie" name="kie" class="form-control" placeholder="KIE">
                                    <div class="input-group-append">

                                    </div>
                                </div>



                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>


                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->
        <script>
            n = new Date();
            y = n.getFullYear();
            m = n.getMonth() + 1;
            d = n.getDate();
            document.getElementById("date").value = y + "/" + m + "/" + d;
        </script>
        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop