@extends('head')

@section('tittle', 'Edit Nifas')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Edit Data laporan Ibu Nifas</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/nifas/edit" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $nifas->id }}">
                                <tr>
                                    <td><b>Nama</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $nifas->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tanggal</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="date" disabled id="tanggal" name="tanggal" value="{{ $nifas->tanggal }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Subject</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="subject" name="subject" value="{{ $nifas->subject }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <tr>
                                    <td><b>Tekanan Darah</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tekanan_darah" name="tekanan_darah" value="{{ $nifas->tekanan_darah }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Nadi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="nadi" name="nadi" value="{{ $nifas->nadi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Suhu</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" value="{{ $nifas->suhu }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>CU</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="cu" name="cu" value="{{ $nifas->cu }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>TFU</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tfu" name="tfu" value="{{ $nifas->tfu }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Pendarahan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="pendarahan" name="pendarahan" value="{{ $nifas->pendarahan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Lochea</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="lochea" name="lochea" value="{{ $nifas->lochea }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>BAB</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="bab" name="bab" value="{{ $nifas->bab }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>BAK</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="bak" name="bak" value="{{ $nifas->bak }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Asi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="asi" name="asi" value="{{ $nifas->asi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Post Partum</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="post_partum" name="post_partum" value="{{ $nifas->post_partum }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>TX</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" value="{{ $nifas->tx }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>KIE</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="kie" name="kie" value="{{ $nifas->kie }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>


                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop