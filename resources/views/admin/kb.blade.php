@extends('admin/mainadmin')

@section('tittle', 'Laporan Harian KB')

@section('cont')
<section id="kb" class="services">
    <div class="container">

        <div class="section-title" data-aos="zoom-out" style="margin-top:6%;">
            <h2>Data</h2>
            <p>Laporan Harian KB</p>
        </div>
        <form action="{{ url()->current() }}" method="get">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">

                        <input type="search" class="form-control" autocomplete="false" name="keyword" value="{{ request('keyword') }}" placeholder="Cari Nama Pasien">

                    </div>
                    <div class="col-md-4" style="padding-bottom: 20px;">
                        <button type="submit" class="btn" style="background-color: #153963; color:white">Cari Nama Pasien
                        </button>

                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <input placeholder="Pilih Tahun" name="tahun"  autocomplete="off" style="width: 100%;" class='datepicker' />
                            </div>

                            <div class="col-md-3">
                                <input type="submit" class="btn btn-success" formaction="/laporan/kb/export" value="Export Data">

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </form>
        <!-- <a class="btn btn-success" href="/laporan/kb/export">Export</a> -->

        <div class="container">
            <!-- <a href="/laporan/kb/tambah"> + Tambah Data Laporan KB</a> -->
            <div class="col-lg-20">
                <div style="overflow: auto;">

                    <table class="table table-striped ">
                        <thead>
                            <tr>

                                <th scope="col">Tanggal</th>
                                <th scope="col">Nama Pasien</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Umur</th>
                                <th scope="col">Subject</th>
                                <th scope="col">BB</th>
                                <th scope="col">TD</th>
                                <th scope="col">Akseptor KB</th>
                                <th scope="col">TX</th>
                                <th scope="col">KIE</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kb as $k)
                            <tr>
                                <td>{{$k->tanggal}}</td>
                                <td>{{$k->nama}}</td>
                                <td>{{$k->alamat}}</td>
                                <td>{{$k->tanggal_lahir}}</td>
                                <td>{{$k->subject}}</td>
                                <td>{{$k->berat_badan}}</td>
                                <td>{{$k->tekanan_darah}}</td>
                                <td>{{$k->akseptor_kb}}</td>
                                <td>{{$k->tx}}</td>
                                <td>{{$k->kie}}</td>
                                <td>
                                    <a href="/laporan/kb/edit/{{ $k->id }}">Edit</a>
                                    |
                                    <a class="btn btn-danger" data-toggle="modal" id="smallButton" data-target="#smallModal" data-attr="/laporan/kb/hapus/{{ $k->id }}" title="Delete Project">
                                        Hapus
                                    </a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
</section>
@endsection

<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.min.css">
<style>
    /* Style to hide Dates / Months */
    .ui-datepicker-calendar,
    .ui-datepicker-month {
        display: none;
    }
</style>

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script> -->
<script>
    $(function() {
        $('.datepicker').datepicker({
            changeMonth: false,
            changeYear: true,
            showButtonPanel: true,

            dateFormat: 'yy',
            onClose: function(dateText, inst) {
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, 0, 1));
            }
        });
    });
</script>

<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="smallBody">
                <div>
                    <!-- the result to be displayed apply here -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css"> -->
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
<script>
    // display a modal (small modal)
    $(function() {
        $('#datepicker1').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            }
        });
    });

    $(document).on('click', '#smallButton', function(event) {
        event.preventDefault();
        let href = $(this).attr('data-attr');
        $.ajax({
            url: href,
            beforeSend: function() {
                $('#loader').show();
            },
            // return the result
            success: function(result) {
                $('#smallModal').modal("show");
                $('#smallBody').html(result).show();
            },
            complete: function() {
                $('#loader').hide();
            },
            error: function(jqXHR, testStatus, error) {
                console.log(error);
                alert("Page " + href + " cannot open. Error:" + error);
                $('#loader').hide();
            },
            timeout: 8000
        })
    });
</script>