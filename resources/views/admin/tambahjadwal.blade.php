@extends('head')

@section('tittle', 'Tambah Jadwal')

<body style="background-color: #153963;" class="hold-transition login-page">

    <div style="height: 10%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Tambah Data Jadwal Kontrol</b>
                        </div>
                        <div class="card-body">

                            <form action="/jadwal/tambah" method="post">
                                {{ csrf_field() }}
                                <div class="input-group mb-3">
                                    <select class="custom-select custom-select-md mb-3" id="id_pasien" name="id_pasien" aria-label="Default select example">
                                        @foreach($pasien as $p)
                                        <option value="{{ $p->id }}">{{ $p->nama}}</option>

                                        @endforeach
                                    </select>
                                    <!-- <input required type="text" id="nama" name="nama" class="form-control" placeholder="Nama"> -->

                                </div>

                                <div class="input-group mb-3">
                                    <input required type="date" id="tanggal" name="tanggal" class="form-control" placeholder="tanggal">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <input required type="time" id="jam" name="jam" class="form-control" placeholder="jam">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <div class="input-group mb-3">
                                    <select class="custom-select custom-select-md mb-3" id="jenisperiksa" name="jenisperiksa" aria-label="Default select example">
                                        <option>Periksa Kehamilan</option>
                                        <option>Keluarga Berencana</option>
                                    </select>

                                </div>
                                <div class="input-group mb-3">
                                    <input required type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>


                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop