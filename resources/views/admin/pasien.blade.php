@extends('admin/mainadmin')

@section('tittle', 'Pasien')

@section('cont')
<section id="pasien" class="services">
    <div class="container">

        <div class="section-title" data-aos="zoom-out" style="margin-top:6%;">
            <h2>Data</h2>
            <p>Registrasi Pasien</p>
        </div>

        <form action="{{ url()->current() }}" method="get">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">

                        <input type="search" class="form-control" autocomplete="false" name="keyword" value="{{ request('keyword') }}" placeholder="Cari Nama Pasien">

                    </div>
                    <div class="col-md-4">
                        <button type="submit" class="btn" style="background-color: #153963; color:white">Cari Nama Pasien
                        </button>

                    </div>
                    <div class="col-md-5">

                    </div>

                </div>

            </div>
        </form>

        <div class="container">
            <div class="col-lg-20">
                <div style="overflow: auto;">

                    <table class="table table-striped ">
                        <thead>
                            <tr>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Umur</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pasien as $p)
                            <tr>
                                <td>{{$p->nama}}</td>
                                <td>{{$p->email}}</td>
                                <td>{{$p->alamat}}</td>
                                <td>{{$p->tahun}}</td>
                                <!-- <td> -->
                                <!-- <a href="/pasien/edit/{{ $p->id }}">Edit</a> -->
                                <td>
                                    <a class="btn btn-primary" data-toggle="modal" id="smallButton" data-target="#smallModal" data-attr="/pasien/detail/{{ $p->id }}" data-attr2="{{$p->nama}}" title="lihatDetail">
                                        Detail Pemeriksaan
                                    </a>
                                </td>

                                <!-- <form action="/pemeriksaan/{{$p->id}}" method="post">
                                {{ csrf_field() }}
                                    <button type="submit" name="your_name" value="your_value" class="btn-link">Go</button>
                                </form> -->
                                <!-- <a href="pemeriksaan/{{$p->id}}">Lihat Detail</a> -->
                                <!-- </td> -->
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
</section>
@endsection



<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="text-align: left;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h3 style="padding-right: 50%;" id="judul"></h3>
            </div>
            <div class="modal-body" id="smallBody">
                <div>
                    <!-- the result to be displayed apply here -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    // display a modal (small modal)
    $(document).on('click', '#smallButton', function(event) {
        event.preventDefault();
        let href = $(this).attr('data-attr');
        var str = 'Catatan Pemeriksaan - ' + $(this).attr('data-attr2');
        $("#judul").html(str);
        $.ajax({
            url: href,
            beforeSend: function() {
                $('#loader').show();
            },
            // return the result
            success: function(result) {
                $('#smallModal').modal("show");
                $('#smallBody').html(result).show();
            },
            complete: function() {
                $('#loader').hide();
            },
            error: function(jqXHR, testStatus, error) {
                console.log(error);
                alert("Page " + href + " cannot open. Error:" + error);
                $('#loader').hide();
            },
            timeout: 8000
        })
    });
</script>