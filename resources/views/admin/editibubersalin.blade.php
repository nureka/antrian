@extends('head')
@section('tittle', 'Edit Ibu Bersalin')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Edit Data laporan Ibu Bersalin</b>
                        </div>
                        <div class="card-body">

                            <form action="/laporan/ibubersalin/edit" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $ibubersalin->id }}">
                                <tr>
                                    <td><b>Nama</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $ibubersalin->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tanggal</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="date" disabled id="tanggal" name="tanggal" value="{{ $ibubersalin->tanggal }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Partograf</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="partograf" name="partograf" value="{{ $ibubersalin->partograf }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Kala 1</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="kala_1" name="kala_1" value="{{ $ibubersalin->kala_1 }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Proses Persalinan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="proses_persalinan" name="proses_persalinan" value="{{ $ibubersalin->proses_persalinan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Tekanan Darah</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tekanan_darah" name="tekanan_darah" value="{{ $ibubersalin->tekanan_darah }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Nadi</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="nadi" name="nadi" value="{{ $ibubersalin->nadi }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Suhu</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="suhu" name="suhu" value="{{ $ibubersalin->suhu }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>CU</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="cu" name="cu" value="{{ $ibubersalin->cu }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Perineum</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="perineum" name="perineum" value="{{ $ibubersalin->perineum }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Perdarahan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="perdarahan" name="perdarahan" value="{{ $ibubersalin->perdarahan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>TX</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="tx" name="tx" value="{{ $ibubersalin->tx }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>BBL</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="bbl" name="bbl" value="{{ $ibubersalin->bbl }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>BB</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="bb" name="bb" value="{{ $ibubersalin->bb }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>PB</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="pb" name="pb" value="{{ $ibubersalin->pb }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>LIKA</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="lika" name="lika" value="{{ $ibubersalin->lika }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>IMD</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input type="text" id="imd" name="imd" value="{{ $ibubersalin->imd }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop