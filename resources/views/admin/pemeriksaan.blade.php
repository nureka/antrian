@extends('admin/mainadmin')

@section('tittle', 'Pemeriksaan')

@section('cont')
<section id="pasien" class="services">
    <div class="container">

        <div class="section-title" data-aos="zoom-out" style="margin-top:6%;">
            <h2>Data</h2>
            <p>Pasien</p>
        </div>

        <div class="container">
            <div class="col-lg-20">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Keadaan Umum</th>
                            <th scope="col">Berat Badan</th>
                            <th scope="col">Tekanan Darah</th>
                            <th scope="col">Suhu</th>
                            <th scope="col">Nadi</th>
                            <th scope="col">Denyut Jantung</th>
                            <th scope="col">Tinggi Fundus Uteri</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pemeriksaan as $p)
                        <tr>
                            <td>{{$p->tanggal}}</td>
                            <td>{{$p->keadaan_umum}}</td>
                            <td>{{$p->berat_badan}}</td>
                            <td>{{$p->tekanan_darah}}</td>
                            <td>{{$p->suhu}}</td>
                            <td>{{$p->nadi}}</td>
                            <td>{{$p->denyut_jantung}}</td>
                            <td>{{$p->tinggi_fundus_uteri}}</td>
                            <td>
                                <a href="/pemeriksaan/edit/{{ $p->id }}">Edit</a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
</section>
@endsection



<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="smallBody">
                <div>
                    <!-- the result to be displayed apply here -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    // display a modal (small modal)
    $(document).on('click', '#smallButton', function(event) {
        event.preventDefault();
        let href = $(this).attr('data-attr');
        $.ajax({
            url: href,
            beforeSend: function() {
                $('#loader').show();
            },
            // return the result
            success: function(result) {
                $('#smallModal').modal("show");
                $('#smallBody').html(result).show();
            },
            complete: function() {
                $('#loader').hide();
            },
            error: function(jqXHR, testStatus, error) {
                console.log(error);
                alert("Page " + href + " cannot open. Error:" + error);
                $('#loader').hide();
            },
            timeout: 8000
        })
    });
</script>