@extends('head')

@section('tittle', 'Edit Jadwal')

<body style="background-color: #153963;" class="hold-transition login-page">
    <div style="height: 5%;">

    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-box">
                    <!-- /.login-logo -->
                    <div class="card card-outline">
                        <div class="card-header text-center">
                            <b>Edit Data Jadwal Kontrol</b>
                        </div>
                        <div class="card-body">

                            <form action="/jadwal/edit" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $jadwal->id }}">
                                <tr>
                                    <td><b>Nama</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="text" disabled id="nama" name="nama" value="{{ $jadwal->nama }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Tanggal</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="date" id="tanggal" name="tanggal" value="{{ $jadwal->tanggal }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>

                                <tr>
                                    <td><b>Jam</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="time" id="jam" name="jam" value="{{ $jadwal->jam }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <tr>
                                    <td><b>Jenis Periksa</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <select class="custom-select custom-select-md mb-3" id="jenisperiksa" name="jenisperiksa" aria-label="Default select example">
                                        <option>Periksa Kehamilan</option>
                                        <option>Keluarga Berencana</option>
                                    </select>

                                </div>
                                <tr>
                                    <td><b>Keterangan</b></td>
                                </tr>
                                <div class="input-group mb-3">
                                    <input required type="text" id="keterangan" name="keterangan" value="{{ $jadwal->keterangan }}" class="form-control">
                                    <div class="input-group-append">

                                    </div>
                                </div>
                                <div class="row">

                                    <!-- /.col -->
                                    <div class="col-4">
                                        <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Simpan</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.login-box -->
                </div>

                <div class="col-md-3"></div>
            </div>

        </div>
        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop