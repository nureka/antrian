@extends('admin/mainadmin')

@section('tittle', 'Antrian')

@section('cont')
<section id="pasien" class="services">
    <div class="container">

        <div class="section-title" data-aos="zoom-out" style="margin-top:6%;">
            <h2>Antrian</h2>
            <p>Hari Ini</p>
        </div>

        <div class="container">
            <div class="col-lg-20">
                <div style="overflow: auto;">
                    <table class="table table-striped ">
                        <thead>
                            <tr>
                                <th scope="col">ID Pasien</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Jenis Periksa</th>
                                <th scope="col">Jam</th>
                                <th scope="col">No Antrian</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($antrian as $a)
                            <tr>
                                <td>{{$a->id_pasien}}</td>
                                <td>{{$a->tanggal}}</td>
                                <td>{{$a->jenisperiksa}}</td>
                                <td>{{$a->jam}}</td>
                                <td>{{$a->no_antrian}}</td>
                                <td>{{$a->status}}</td>
                                <td>
                                    @if($a->status != 'sudah diperiksa' && $a->status != 'Batal Diperiksa')
                                    @if($a->status!="Sedang Diperiksa")
                                    <a href="/antrian/updatestatus/{{$a->id}}">Panggil</a>

                                    |
                                    <a class="btn btn-danger" data-toggle="modal" id="smallButton" data-target="#smallModal" data-attr="/antrian/batal/{{$a->id}}" title="Delete Project">
                                        Batal
                                    </a>
                                    <!-- <a href="/antrian/batal/{{$a->id}}">
                                        Batal
                                    </a> -->
                                    @endif


                                    @if ($a->jenisperiksa=='ibu_hamil' && $a->status!='')
                                    <a href="/laporan/ibuhamil/tambah/{{$a->id_pasien}}/{{$a->id}}">Assestment</a>
                                    @elseif ($a->jenisperiksa=='periksa_anak' && $a->status!='')
                                    <a href="/laporan/anak/tambah/{{$a->id_pasien}}/{{$a->id}}">Assestment</a>
                                    @elseif ($a->jenisperiksa=='periksa_kb' && $a->status!='')
                                    <a href="/laporan/kb/tambah/{{$a->id_pasien}}/{{$a->id}}">Assestment</a>
                                    @endif
                                    @endif
                                </td>

                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
</section>
@endsection



<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="smallBody">
                <div>
                    <!-- the result to be displayed apply here -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    // display a modal (small modal)
    $(document).on('click', '#smallButton', function(event) {
        event.preventDefault();
        let href = $(this).attr('data-attr');
        $.ajax({
            url: href,
            beforeSend: function() {
                $('#loader').show();
            },
            // return the result
            success: function(result) {
                $('#smallModal').modal("show");
                $('#smallBody').html(result).show();
            },
            complete: function() {
                $('#loader').hide();
            },
            error: function(jqXHR, testStatus, error) {
                console.log(error);
                alert("Page " + href + " cannot open. Error:" + error);
                $('#loader').hide();
            },
            timeout: 8000
        })
    });
</script>