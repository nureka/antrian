@extends('head')

<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <b>Edit Data Pemeriksaan</b>
            </div>
            <div class="card-body">

                <form action="/pemeriksaan/edit" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $pemeriksaan->id }}">
                    <tr>
                        <td><b>Tanggal</b></td>
                    </tr>
                    <div class="input-group mb-3">

                        <input required type="text" disabled id="tanggal" name="tanggal" value="{{ $pemeriksaan->tanggal }}" class="form-control">
                        <div class="input-group-append">
                            <!-- <div class="input-group-text">
                            </div> -->
                        </div>
                    </div>
                    <tr>
                        <td><b>Keadaan Umum</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="text" id="keadaan_umum" name="keadaan_umum" value="{{ $pemeriksaan->keadaan_umum }}" class="form-control">
                        <div class="input-group-append">
                            <!-- <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div> -->
                        </div>
                    </div>

                    <tr>
                        <td><b>Berat Badan</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="number" id="berat_badan" name="berat_badan" value="{{ $pemeriksaan->berat_badan }}" class="form-control">
                        <div class="input-group-append">
                            <!-- <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div> -->
                        </div>
                    </div>

                    <tr>
                        <td><b>Tekanan Darah</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="text" id="tekanan_darah" name="tekanan_darah" value="{{ $pemeriksaan->tekanan_darah }}" class="form-control">
                        <div class="input-group-append">
                            <!-- <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div> -->
                        </div>
                    </div>

                    <tr>
                        <td><b>Suhu</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="text" id="suhu" name="suhu" value="{{ $pemeriksaan->suhu }}" class="form-control">
                        <div class="input-group-append">
                            <!-- <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div> -->
                        </div>
                    </div>

                    <tr>
                        <td><b>Nadi</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="text" id="nadi" name="nadi" value="{{ $pemeriksaan->nadi }}" class="form-control">
                        <div class="input-group-append">
                            <!-- <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div> -->
                        </div>
                    </div>

                    <tr>
                        <td><b>Denyut Jantung</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="text" id="denyut_jantung" name="denyut_jantung" value="{{ $pemeriksaan->denyut_jantung }}" class="form-control">
                        <div class="input-group-append">
                            <!-- <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div> -->
                        </div>
                    </div>

                    <tr>
                        <td><b>Tinggi Fundus Uteri</b></td>
                    </tr>
                    <div class="input-group mb-3">
                        <input required type="text" id="tinggi_fundus_uteri" name="tinggi_fundus_uteri" value="{{ $pemeriksaan->tinggi_fundus_uteri }}" class="form-control">
                        <div class="input-group-append">
                            <!-- <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div> -->
                        </div>
                    </div>
                    <div class="row">

                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.login-box -->

        <!--     -->

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')
        <script>
            console.log('Hi!');
        </script>
        @stop