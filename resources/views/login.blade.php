@extends('head')
@section('tittle', 'Login')

<body style="background-color: #153963;" class="hold-transition login-page">
  <div style="height: 30%;">

  </div>
  <div class="container">
    <div class="row">

      <div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="login-box">
          <!-- /.login-logo -->
          <div class="card card-outline">
            <div class="card-header text-center">
              <b>Sistem Antrian PMB Yuliani</b>
            </div>
            <div class="card-body">

              <form action="/dashboard" method="post">
                @csrf
                <div class="input-group mb-3">
                  <input required type="email" id="email" name="email" class="form-control" placeholder="Email">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-envelope"></span>
                    </div>
                  </div>
                </div>
                <div class="input-group mb-3">
                  <input required type="password" id="password" name="password" class="form-control" placeholder="Password">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-lock"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  @if(isset($data_))
                  <div class="col-12">
                    <div class="alert alert-danger">
                      <strong>{{ $data_ }}</strong>
                    </div>
                  </div>
                  @endif
                  <!-- /.col -->
                  <div class="col-4">
                    <button type="submit" style="background-color: #153963; color:white" class="btn btn-block">Sign In</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>


              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.login-box -->

        </div>

        <div class="col-md-4"></div>
      </div>

    </div>

    <!--     -->

    @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

    @section('js')
    <script>
      console.log('Hi!');
    </script>
    @stop