@extends('head')
@section('tittle', 'Daftar Antrian')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
  Launch static backdrop modal
</button> -->

<script src="https://kit.fontawesome.com/267c0be21d.js" crossorigin="anonymous"></script>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="container-fluid">
        <div style="height: 20px;"></div>
        <div class="row d-flex justify-content-center">
            <form autocomplete="off" method="post" action="/antrian/daftarPemeriksaan">
                @csrf
                <div class="col-md-12">
                    <div class="card-head">
                        <strong>
                            <p>Pilih Layanan</p>
                        </strong>
                    </div>
                    <div class="card-body">
                        <label class="card-radio-btn">
                            <input type="radio" name="jenis_periksa" class="card-input-element d-none" id="ibu_hamil" value="ibu_hamil" checked="">
                            <div class="card card-body">
                                <div class="content_head">Pemeriksaan Ibu Hamil</div>
                                <div class="content_sub">untuk kontrol / periksa kandungan</div>
                                <!-- <p> included </p> -->
                            </div>
                        </label>
                        <label class="card-radio-btn">
                            <input type="radio" name="jenis_periksa" class="card-input-element d-none" id="periksa_kb" value="periksa_kb">
                            <div class="card card-body">
                                <!-- <div class="ribbon"><span style="color:#22202d;">suggested</span></div> -->
                                <div class="content_head">Pemeriksaan KB</div>
                                <div class="content_sub">untuk pelayanan periksa kb</div>
                                <!-- <p> +20€ </p> -->
                            </div>
                        </label>
                    </div>
                    <div class="card-head">
                        <strong>
                            <p>Pilih Tanggal</p>
                        </strong>
                    </div>
                    <div>
                        <input type="text" class="date form-control" name="tanggal" id="tanggal" placeholder="Masukkan Tgl Periksa" required>
                    </div>
                    <div style="height: 20px;"></div>
                    <div>
                        <button type="submit" class="btn btn-success">Daftar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</body>
<script type="text/javascript">
    $('.date').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        endDate: new Date(new Date().setDate(new Date().getDate() + 1))
    });
</script>
<style>
    .card {
        border: 2px solid rgba(0, 0, 0, 0.1);
        border-radius: 0.65rem;
    }

    hr.style-1 {
        margin-top: 1rem;
        margin-bottom: 1rem;
        border: 0;
        border-top: 2px solid rgba(0, 0, 0, 0.07);
    }

    .fw-500 {
        font-weight: 400;
    }

    .fw-600 {
        font-weight: 500;
    }

    .mt_2 {
        margin-top: 2rem !important;
    }

    /*Simple radio btn input CSS*/
    .custom-control-label {
        cursor: pointer;
    }

    .cap-opt-1 {
        display: inline-block;
        min-width: 50px;
    }

    .custom-control-input:checked~.custom-control-label::before {
        color: #fff;
        border-color: #59f75c;
        background-color: #59f75c;
    }

    /*Card Button CSS*/

    .card-radio-btn .content_head {
        color: #333;
        font-size: 1.2rem;
        line-height: 30px;
        font-weight: 500;
    }

    .card-radio-btn .content_sub {
        color: #9e9e9e;
        font-size: 14px;
    }

    .card-input-element+.card {
        width: 300px;
        height: 165px;
        margin: 10px;
        justify-content: center;
        color: var(--primary);
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 2px solid transparent;
        border-radius: 10px;
        text-align: center;
        -webkit-box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.1);
        box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.1);
    }

    .card-input-element+.card:hover {
        cursor: pointer;
    }

    .card-input-element:checked+.card {
        border: 2px solid #77ca71;
        -webkit-transition: border 0.3s;
        -o-transition: border 0.3s;
        transition: border 0.3s;
    }

    .card-input-element:checked+.card::after {
        content: "\f058";
        color: rgb(120 201 112);
        position: absolute;
        right: 5px;
        top: 5px;
        font-family: "Font Awesome 5 Free";
        font-style: normal;
        font-size: 1rem;
        font-weight: 900;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        -webkit-animation-name: fadeInCheckbox;
        animation-name: fadeInCheckbox;
        -webkit-animation-duration: 0.3s;
        animation-duration: 0.3s;
        -webkit-animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
    }

    @-webkit-keyframes fadeInCheckbox {
        from {
            opacity: 0;
            -webkit-transform: rotateZ(-20deg);
        }

        to {
            opacity: 1;
            -webkit-transform: rotateZ(0deg);
        }
    }

    @keyframes fadeInCheckbox {
        from {
            opacity: 0;
            transform: rotateZ(-20deg);
        }

        to {
            opacity: 1;
            transform: rotateZ(0deg);
        }
    }

    .ribbon {
        position: absolute;
        top: 5px;
        left: 5px;
        background-color: #ffcc00;
        padding: 3px;
        border-radius: 10px;
        font-size: 0.8rem;
        width: 90px;
    }
</style>

@section('js')
<script>
    console.log('Hi!');
    var myVar = setInterval(myTimer, 1000);

    function myTimer() {
        var d = new Date();
        let ye = new Intl.DateTimeFormat('en', {
            year: 'numeric'
        }).format(d);
        let mo = new Intl.DateTimeFormat('en', {
            month: 'short'
        }).format(d);
        let da = new Intl.DateTimeFormat('en', {
            day: '2-digit'
        }).format(d);
        document.getElementById("jam_sekarang").innerHTML = "Sistem antrian PMB Yuliani " + `${da}-${mo}-${ye}` + ' ' + d.toLocaleTimeString() + "";
    };
</script>
@stop