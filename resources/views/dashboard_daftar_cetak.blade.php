@extends('head')
@section('tittle', 'Cetak Antrian')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">

<body class="hold-transition sidebar-mini layout-fixed A5">
    <div class="container-fluid">
        <div style="height: 20px;"></div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <div class="card">
                    <div class="card-header ">
                        <h5 class="card-title m-0">Pendaftaran</h5>
                    </div>
                    <div class="card-body">

                        <h6 class="card-title">Anda Mendapat nomor : {{$antrian['no_antrian']}}</h6>
                        <p class="card-text">Jenis Pemeriksaan : {{$antrian['jenisperiksa']}}</p>
                        <p class="card-text">Estimasi Waktu Diperiksa : {{$antrian['tanggal']}}, {{$antrian['jam']}}</p>
                    </div>
                </div>

            </div>

            <div class="col-md-4"></div>

        </div>
    </div>

</body>
<style>
    @page {
        size: A5
    }
</style>
<script>
    window.print();
</script>