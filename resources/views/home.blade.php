@extends('head')
@section('tittle', 'Home')

<body style="background-color: #153963;" class="hold-transition login-page">
  <div style="height: 30%;">

  </div>
  <div class="container">
    <div class="row">

      <div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="login-box">
          <!-- /.login-logo -->
          <div class="card card-outline">
            <div class="card-header text-center">
              <b>Sistem Antrian PMB Yuliani</b>
            </div>
            <div class="card-body">

              <div class="row">

                <!-- /.col -->
                <div class="col-6">
                  <a href="{{route('login')}}" style="background-color: #153963; color:white" class="btn btn-block">Sign In</a>
                </div>
                <div class="col-6">
                  <a href="{{route('register')}}" style="background-color: #153963; color:white" class="btn btn-block">Register</a>
                </div>
                <!-- /.col -->
              </div>


              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.login-box -->
        </div>

        <div class="col-md-4"></div>
      </div>

    </div>


    <!--     -->

    @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

    @section('js')
    <script>
      console.log('Hi!');
    </script>
    @stop