<?php

use App\Http\Controllers\AnakController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PasienController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\AntrianController;
use App\Http\Controllers\BayiBLController;
use App\Http\Controllers\IbuBersalinController;
use App\Http\Controllers\IbuHamilController;
use App\Http\Controllers\KBController;
use App\Http\Controllers\NifasController;
use App\Http\Controllers\PemeriksaanController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Spatie\GoogleCalendar\Event;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('home');
})->name('home')->middleware('guest');

Route::get('/tes', function () {
    return view('quickstart');
})->name('home2');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/register', function () {
    return view('register');
})->name('register');

Route::get('/admin/dashboard', function () {
    return view('admin/dashboard');
});

Route::get('/logout', [LoginController::class, 'logout']);

Route::get('/dashboard', [LoginController::class, 'index'])->name('dashboard_pasien');
Route::post('/dashboard',  [LoginController::class, 'dologin'])->name('dashboard');
Route::get('/dashboard_daftar',  [AntrianController::class, 'callback'])->name('dashboard_daftar');
Route::post('/admin/dashboard',  [LoginController::class, 'login'])->name('dashboard_admin');
//Route::get('/admin/dashboard',  [LoginController::class, 'index']);
Route::post('/registers',  [RegisterController::class, 'register'])->name('registers');

// Route::get('/', [LoginController::class, 'index']);
Route::group(['prefix' => '/', 'middleware' => 'auth'], function () {
    //admin pasien
    // Route::get('/', [LoginController::class, 'index']);
    Route::get('/pasien', [PasienController::class, 'index']);
    Route::post('/pasien/tambah', [PasienController::class, 'store']);
    Route::post('/pasien/tambahAnak', [PasienController::class, 'storeAnak']);
    Route::get('/pasienadm/tambah', [PasienController::class, 'tambahpasien']);
    Route::get('/pasien/edit/{id}', [PasienController::class, 'update']);
    Route::get('/pasien/detail/{id}', [PasienController::class, 'detail']);
    Route::post('/pasien/edit', [PasienController::class, 'editpasien']);
    Route::get('/pasien/hapus/{id}', [PasienController::class, 'delete']);
    Route::get('/pasien/destroy/{id}', [PasienController::class, 'destroy']);
    Route::get('/pasien/destroy/{id}', [PasienController::class, 'destroy']);

    ////admin jadwal
    Route::get('/jadwal', [JadwalController::class, 'index']);
    Route::get('/jadwal_kontrol', [JadwalController::class, 'jadwal']);
    Route::get('/data_jadwal_kontrol', [JadwalController::class, 'jadwalKontrol']);
    Route::post('/jadwal/tambah', [JadwalController::class, 'store']);
    Route::get('/jadwaladm/tambah', [JadwalController::class, 'tambahjadwal']);
    Route::get('/jadwal/edit/{id}', [JadwalController::class, 'update']);
    Route::post('/jadwal/edit', [JadwalController::class, 'editjadwal']);
    Route::get('/jadwal/hapus/{id}', [JadwalController::class, 'delete']);
    Route::get('/jadwal/destroy/{id}', [JadwalController::class, 'destroy']);

    //admin antrian

    Route::get('/data_dashboard', [JadwalController::class, 'dataDashboard']);
    Route::get('/data_dashboard_kelahiran', [JadwalController::class, 'dataDashboard_kelahiran']);
    Route::get('/antrian', [AntrianController::class, 'index']);
    Route::get('/daftar_antrian', [AntrianController::class, 'create']);
    Route::get('/daftar_antrian_anak/{id}', [AntrianController::class, 'create_anak']);
    Route::get('/cetak/{data}', [AntrianController::class, 'callback_cetak']);
    Route::post('/antrian/daftarPemeriksaan', [AntrianController::class, 'store']);
    Route::get('/antrian/updatestatus/{id}', [AntrianController::class, 'updatestatus']);
    Route::get('/antrian/batal/{id}', [AntrianController::class, 'hapus']);
    Route::get('/antrian/batal_/{id}', [AntrianController::class, 'batal']);


    Route::get('/pemeriksaan', [PemeriksaanController::class, 'index']);
    Route::get('/pemeriksaan/{id}', [PemeriksaanController::class, 'show']);
    Route::get('/pemeriksaan/detail', [PemeriksaanController::class, 'detail']);
    Route::post('/pemeriksaan/tambah', [PemeriksaanController::class, 'store']);
    Route::get('/pemeriksaan/tambah/{id}', [PemeriksaanController::class, 'tambahpemeriksaan']);
    Route::get('/pemeriksaan/edit/{id}', [PemeriksaanController::class, 'update']);
    Route::post('/pemeriksaan/edit', [PemeriksaanController::class, 'editpemeriksaan']);

    //admin laporan harian
    Route::get('/laporan/ibuhamil', [IbuHamilController::class, 'index']);
    Route::post('/laporan/ibuhamil/tambah', [IbuHamilController::class, 'store']);
    Route::get('/laporan/ibuhamil/tambah/{id}/{id_antrian}', [IbuHamilController::class, 'tambahibuhamil']);
    Route::get('/laporan/ibuhamil/edit/{id}', [IbuHamilController::class, 'update']);
    Route::post('/laporan/ibuhamil/edit', [IbuHamilController::class, 'editibuhamil']);
    Route::get('/laporan/ibuhamil/hapus/{id}', [IbuHamilController::class, 'delete']);
    Route::get('/laporan/ibuhamil/destroy/{id}', [IbuHamilController::class, 'destroy']);
    Route::get('/laporan/ibuhamil/export', [IbuHamilController::class, 'export']);

    Route::get('/laporan/ibubersalin', [IbuBersalinController::class, 'index']);
    Route::post('/laporan/ibubersalin/tambah', [IbuBersalinController::class, 'store']);
    Route::get('/laporan/ibubersalin/tambah', [IbuBersalinController::class, 'tambahibubersalin']);
    Route::get('/laporan/ibubersalin/edit/{id}', [IbuBersalinController::class, 'update']);
    Route::post('/laporan/ibubersalin/edit', [IbuBersalinController::class, 'editibubersalin']);
    Route::get('/laporan/ibubersalin/hapus/{id}', [IbuBersalinController::class, 'delete']);
    Route::get('/laporan/ibubersalin/destroy/{id}', [IbuBersalinController::class, 'destroy']);
    Route::get('/laporan/ibubersalin/export', [IbuBersalinController::class, 'export']);

    Route::get('/laporan/nifas', [NifasController::class, 'index']);
    Route::post('/laporan/nifas/tambah', [NifasController::class, 'store']);
    Route::get('/laporan/nifas/tambah', [NifasController::class, 'tambahnifas']);
    Route::get('/laporan/nifas/edit/{id}', [NifasController::class, 'update']);
    Route::post('/laporan/nifas/edit', [NifasController::class, 'editnifas']);
    Route::get('/laporan/nifas/hapus/{id}', [NifasController::class, 'delete']);
    Route::get('/laporan/nifas/destroy/{id}', [NifasController::class, 'destroy']);
    Route::get('/laporan/nifas/export', [NifasController::class, 'export']);

    Route::get('/laporan/kb', [KBController::class, 'index']);
    Route::post('/laporan/kb/tambah', [KBController::class, 'store']);
    Route::get('/laporan/kb/tambah/{id}/{id_antrian}', [KBController::class, 'tambahkb']);
    Route::get('/laporan/kb/edit/{id}', [KBController::class, 'update']);
    Route::post('/laporan/kb/edit', [KBController::class, 'editkb']);
    Route::get('/laporan/kb/hapus/{id}', [KBController::class, 'delete']);
    Route::get('/laporan/kb/destroy/{id}', [KBController::class, 'destroy']);
    Route::get('/laporan/kb/export', [KBController::class, 'export']);

    Route::get('/laporan/anak', [AnakController::class, 'index']);
    Route::post('/laporan/anak/tambah', [AnakController::class, 'store']);
    Route::get('/laporan/anak/tambah/{id}/{id_antrian}', [AnakController::class, 'tambahanak']);
    Route::get('/laporan/anak/edit/{id}', [AnakController::class, 'update']);
    Route::post('/laporan/anak/edit', [AnakController::class, 'editanak']);
    Route::get('/laporan/anak/hapus/{id}', [AnakController::class, 'delete']);
    Route::get('/laporan/anak/destroy/{id}', [AnakController::class, 'destroy']);
    Route::get('/laporan/anak/export', [AnakController::class, 'export']);

    Route::get('/laporan/bayibl', [BayiBLController::class, 'index']);
    Route::post('/laporan/bayibl/tambah', [BayiBLController::class, 'store']);
    Route::get('/laporan/bayibl/tambah', [BayiBLController::class, 'tambahbayibl']);
    Route::get('/laporan/bayibl/edit/{id}', [BayiBLController::class, 'update']);
    Route::post('/laporan/bayibl/edit', [BayiBLController::class, 'editbayibl']);
    Route::get('/laporan/bayibl/hapus/{id}', [BayiBLController::class, 'delete']);
    Route::get('/laporan/bayibl/destroy/{id}', [BayiBLController::class, 'destroy']);
    Route::get('/laporan/bayibl/export', [BayiBLController::class, 'export']);

    Route::get('/calendar_google', function () {
        // $e = Event::get();
        $event = new Event;

        $event->name = 'coba sharing 2 email';
        $event->description = 'Event description';
        $event->startDateTime = Carbon\Carbon::now();
        $event->endDateTime = Carbon\Carbon::now()->addHour();
        // $event->addAttendee([
        //     'email' => 'john@example.com',
        //     'name' => 'John Doe',
        //     'comment' => 'Lorum ipsum',
        // ]);
        $event->addAttendee(['email' => 'mafudnurkarim@gmail.com']);

        $event->save();

        $e = Event::get();
        dd($e);
    });
});
